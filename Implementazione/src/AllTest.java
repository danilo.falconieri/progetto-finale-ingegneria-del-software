import appprevisione.AppPrevisioniTest;
import appprevisione.SensoreTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import protezionecivile.ConnettoreDBTest;
import protezionecivile.SistemaCentraleTest;
import user.AppMobileTest;

@RunWith(Suite.class)
@SuiteClasses({ SistemaCentraleTest.class, AppPrevisioniTest.class, ConnettoreDBTest.class, SensoreTest.class, AppMobileTest.class})

public class AllTest {
}
