package appprevisione;

import java.io.Serializable;

public class Allerta implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private int cap;
    private String date;
    private short timeSlot;
    private short level;
    private char type;
    private boolean valid;

    public Allerta(int id, int cap, String date, short timeSlot, short level, char type, boolean valid) {
        this.id = id;
        this.cap = cap;
        this.date = date;
        this.timeSlot = timeSlot;
        this.level = level;
        this.type = type;
        this.valid = valid;
    }

    // GETTERS AND SETTER SECTION
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public short getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(short timeSlot) {
        this.timeSlot = timeSlot;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public boolean getValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return id+";"+cap+";"+date+";"+timeSlot+";"+level+";"+type+";"+valid;
    }
}
