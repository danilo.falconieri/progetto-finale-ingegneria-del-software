package appprevisione;

import serverRMI.ServerSensore;
import tools.Utility;

import static org.junit.Assert.*;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SensoreTest implements Remote {

    @SuppressWarnings("unused")
    @Before
    //Inizializza la connessione col database e avvia il registro RMI
    public void setUp() throws Exception {
        Utility.startRMIRegistry();
    }

    @SuppressWarnings("unused")
    @After
    //Chiude la connessione al database, distrugge testApp e testSystem
    public void tearDown() throws Exception {
        Utility.stopRMIRegistry();
    }

    @SuppressWarnings("unused")
    @Test
    //Verifico range misure Sensore di tipo E
    public void testTypeE() {
        Sensore se = new Sensore(99999, 'E');
        ServerSensore stub = null;
        boolean be = false;
        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            stub = (ServerSensore) registry.lookup("Sensor.99999.E");

            String testDetection = "";
            testDetection = stub.askDetections();

            float me = 0;
            String testMeasure = "";
            String split[] = testDetection.split(";");
            testMeasure = split[4];

            me = Float.valueOf(testMeasure);
            if (me >= 0.1 && me <= 10) {
                be = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(be);
    }

    @SuppressWarnings("unused")
    @Test
    //Verifico range misure Sensore di tipo R
    public void testTypeR() {
        Sensore sr = new Sensore(99998, 'R');
        ServerSensore stub = null;
        boolean br = false;
        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            stub = (ServerSensore) registry.lookup("Sensor.99998.R");

            String testDetection = stub.askDetections();
            float mr = 0;
            String testMeasure = "";
            String split[] = testDetection.split(";");
            testMeasure = split[4];

            mr = Float.valueOf(testMeasure);
            if (mr >= 0.1 && mr <= 70) {
                br = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(br);
    }

    @SuppressWarnings("unused")
    @Test
    //Verifico range misure Sensore di tipo S
    public void testTypeS() {
        Sensore ss = new Sensore(99997, 'S');
        ServerSensore stub = null;
        boolean bs = false;
        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            stub = (ServerSensore) registry.lookup("Sensor.99997.S");

            String testDetection = stub.askDetections();
            float ms = 0;
            String testMeasure = "";
            String split[] = testDetection.split(";");
            testMeasure = split[4];

            ms = Float.valueOf(testMeasure);
            if (ms >= 0.1 && ms <= 4) {
                bs = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(bs);
    }

    @SuppressWarnings("unused")
    @Test
    //Verifico range misure Sensore di tipo W
    public void testTypeW() {
        Sensore sw = new Sensore(99996, 'W');
        ServerSensore stub = null;
        boolean bw = false;
        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            stub = (ServerSensore) registry.lookup("Sensor.99996.W");

            String testDetection = stub.askDetections();
            float mw = 0;
            String testMeasure = "";
            String split[] = testDetection.split(";");
            testMeasure = split[4];

            mw = Float.valueOf(testMeasure);
            if (mw >= 0.1 && mw <= 110) {
                bw = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertTrue(bw);
    }
}
