package appprevisione;

import serverRMI.ServerSensore;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;


public class Sensore implements ServerSensore {
    private String id;
    private char type;
    private float measure;
    private int cap;

    public Sensore(int cap, char type) {
        type = Character.toUpperCase(type);
        this.id = "Sensor." + cap + "." + type;
        this.type = type;
        this.measure = 0;
        this.cap = cap;

        //start RMIServer
        try {
            ServerSensore stub = (ServerSensore) UnicastRemoteObject.exportObject(this, 0);
            Registry reg = LocateRegistry.getRegistry("localhost");
            reg.bind(id, stub);

            System.out.println("Server " + id + " ready");
        } catch (RemoteException | AlreadyBoundException e) {
            System.err.println(id + " Error: cannot bind "+ id +" to rmiregistry (" + e.toString() + ")");
            //  e.printStackTrace();
        }
    }

    //Il metodo detect() consente al sensore di effettuare una rilevazione
    private String detect() {

        Random r = new Random();
        float randomInt;
        int pseudoC = r.nextInt(100);
        
        Calendar calendar = Calendar.getInstance();

        String instant = new SimpleDateFormat("dd-MM-yyyy" + ";" + "HH:mm:ss").format(calendar.getTime());

        switch (type) {

		 /* Range Terromoto: 0.1 -> 10 Scala Richter
	     85% di prob. di rilevare magnitudo tra 0.1 e 4
	     10% di prob. di rilevare magnitudo tra 4.1 e 6
	     5% di prob. di rilevare magnitudo superiore  a 6 */

            case 'E':
                if (pseudoC < 85) {
                    randomInt = r.nextInt(40) + 1;
                } else if (pseudoC >= 85 && pseudoC < 95) {
                    randomInt = r.nextInt(20) + 41;
                } else {
                    randomInt = r.nextInt(40) + 61;
                }

                measure = randomInt / 10;
                break;

             /* Range Pioggia: 0.1 -> 70 mm/h
			 80% di prob. di rilevare pioggia fra 0.1mm e 10mm
			 15% di prob. di rilevare pioggia fra 10.1mm e 30mm
			 5% di prob. di rilevare pioggia fra 30.1 mm e 70mm */

            case 'R':
                if (pseudoC < 80) {
                    randomInt = r.nextInt(100) + 1;
                } else if (pseudoC >= 80 && pseudoC < 95) {
                    randomInt = r.nextInt(200) + 101;
                } else {
                    randomInt = r.nextInt(400) + 301;
                }

                measure = randomInt / 10;
                break;

			/* Range Neve: 0.1 -> 4.0 cm/h
			 85% di prob. di rilevare neve sotto 1.7 cm/h
			 10% di prob. di rilevare neve fra 1.8 cm/h e 2.6 cm/h 
             5% di prob. di rilevare neve sopra i 2.7 cm/h */

            case 'S':
                if (pseudoC < 85) {
                    randomInt = r.nextInt(17) + 1;
                } else if (pseudoC >= 85 && pseudoC < 95) {
                    randomInt = r.nextInt(9) + 18;
                } else {
                    randomInt = r.nextInt(14) + 27;
                }

                measure = randomInt / 10;
                break;

			/* Range Vento: 1 -> 110 km/h
			 70% di prob. di rilevare vento sotto i 45km/h
			 25& di prob. di rilevare vento fra 46km/h e 75km/h
			 5% di prob. di rilevare vento superiore a 76km/h */

            case 'W':
                if (pseudoC < 70) {
                    randomInt = r.nextInt(45) + 1;
                } else if (pseudoC >= 70 && pseudoC < 95) {
                    randomInt = r.nextInt(30) + 46;
                } else {
                    randomInt = r.nextInt(35) + 76;
                }

                measure = randomInt;
                break;

        }

//Restituisce il cap, la tipologia del sensore, l'istante temporale e la misura della rilevazione
        String resultD = "";
        resultD = cap + ";" + type + ";" + instant + ";" + measure;
       
       //Resetto la variabile instant 
        instant = null;

       //Ritorno cap, tipologia, istante e misurazione
        return resultD;
    }

//askDetections ritorna il risultato di detect() e resetta la variabile measure
    @Override
    public String askDetections() {
        String result = "";
        result = detect();
        measure = 0;
        		
        return result;

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public float getMeasure() {
        return measure;
    }

    public void setMeasure(float measure) {
        this.measure = measure;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

}


