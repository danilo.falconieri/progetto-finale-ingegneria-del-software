package appprevisione;

import serverRMI.ServerAppPrevisioni;
import serverRMI.ServerSensore;
import serverRMI.ServerSisCentrale;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class AppPrevisioni implements ServerAppPrevisioni, Runnable {

    private String id;
    private ArrayList<Integer> capList;
    private ArrayList<String> detection = new ArrayList<String>();
    private ArrayList<Allerta> alertList = new ArrayList<Allerta>();
    private ScheduledExecutorService executorService;

    public AppPrevisioni(String id, ArrayList<Integer> capList) {
        this.id = id;
        this.capList = new ArrayList<>(capList);

        // Avvio RMIServerAppMobile
        try {
            ServerAppPrevisioni stub = (ServerAppPrevisioni) UnicastRemoteObject.exportObject(this, 0);
            Registry registry = LocateRegistry.getRegistry("localhost");
            registry.bind(this.id, stub);

            System.out.println("Server " + this.id + " ready");
        } catch (RemoteException | AlreadyBoundException e) {
            System.err.println(this.id + " Error: cannot bind " + this.id + " to rmiregistry (" + e.toString() + ")");
            // e.printStackTrace();
            return;
        }

        // appPrevisione attende che il sistema centrale abbia fatto la bind
        boolean checkSys = false;
        while (!checkSys) {
            try {
                Registry reg = LocateRegistry.getRegistry("localhost");
                ServerSisCentrale stubSystem = (ServerSisCentrale) reg.lookup("SistemaCentrale");
                checkSys = true;
            } catch (RemoteException | NotBoundException e) {
                //e.printStackTrace();
            }
        }
        // per mostrare all'utente dei dati sin dall'inizio
        filter();

        //creo uno ScheduledExecutorService che lavora come un Timer. Inizia con un Delay pari a millisToNextHalfHour() e ripete this.run() ogni 30 minuti
        executorService = Executors.newSingleThreadScheduledExecutor();

        //REAL:
        executorService.scheduleAtFixedRate(this, millisToNextHalfHour(), 1800000, TimeUnit.MILLISECONDS);
    }

    @Override
    public void run() {
        filter();
        System.out.println(id + " IS RUNNING()");
    }

    private long millisToNextHalfHour() {
        //metodo che ritorna la differenza in millisecondi tra l'ora attuale (now) e la mezz'ora successiva spaccata(next)

        LocalTime now = LocalTime.now().truncatedTo(ChronoUnit.MILLIS);
        LocalTime next;

        // siccome in questo caso per fare la differenza con mezzanotte ho bisogno di sapere anche il giorno, uso i LocalDateTime (perch� devo fare la differenza con la mezzanotte del giorno seguente)
        if(now.getHour() == 23 && now.getMinute() > 30) {
        	LocalDateTime nowDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);
        	LocalDateTime nextDateTime = null;
            try {
            	nextDateTime = LocalDateTime.of(nowDateTime.getYear(), nowDateTime.getMonth(), nowDateTime.getDayOfMonth() + 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            } catch (Exception e) {
            	try {
            		nextDateTime = LocalDateTime.of(nowDateTime.getYear(), nowDateTime.getMonth().plus(1), 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);	
            	} catch (Exception e1) {
            		nextDateTime = LocalDateTime.of(nowDateTime.getYear() + 1, Month.JANUARY, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);	
            	}
            }            
            Duration timeElapsed = Duration.between(nowDateTime, nextDateTime);
            return timeElapsed.toMillis();
        }
        
        //fascia hh:00:00 - hh:29:59
        if (now.getMinute() < 30)
            next = LocalTime.of(now.getHour(), 30, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            //fascia hh:30:00 - hh:59:59
        else
            next = LocalTime.of(now.getHour() + 1, 0, 0, 0).truncatedTo(ChronoUnit.MILLIS);

        Duration timeElapsed = Duration.between(now, next);
        return timeElapsed.toMillis();
    }

    private void takeDetections() {
        char[] sensorTypes = {'W', 'R', 'S', 'E'};
        //Prendo tutte le rilevazioni effettuate da tutti i sensori di ogni cap
        for (Integer cap : capList) {
            for (char t : sensorTypes) {
                try {
                    Registry reg = LocateRegistry.getRegistry("localhost");
                    ServerSensore stub = (ServerSensore) reg.lookup("Sensor." + cap + "." + t);
                    detection.add(stub.askDetections());
                } catch (RemoteException | NotBoundException e) {
                    System.err.println(id + " Error: StubSensore for askDetection method (" + e.toString() + ")");
                    // e.printStackTrace();
                }
            }
        }
    }

    protected void generateForecasts() {
        ServerSisCentrale stubSystem = null;
        try {
            Registry reg = LocateRegistry.getRegistry("localhost");
            stubSystem = (ServerSisCentrale) reg.lookup("SistemaCentrale");
        } catch (RemoteException | NotBoundException e1) {
            System.err.println(id + " Error: cannot connect to StubCentralSystem (" + e1.toString() + ")");
            // e.printStackTrace();
        }

        for (String det : detection) {
            //Estraggo tutti i dati contenuti in ogni singola rilevazione
            ArrayList<String> data = new ArrayList<>(Arrays.asList(det.split(";")));
            int cap = Integer.parseInt(data.get(0));
            char type = data.get(1).charAt(0);
            String dateDet = data.get(2);
            String timeDet = data.get(3);
            float measure = Float.parseFloat(data.get(4));

            short level = 0;
            short timeSlotDet = 0;

            //Determino il livello di gravit� per le rilevazioni di tipo "vento"
            if (type == 'W') {
                if (measure >= 50 && measure < 75) {
                    level = 1;
                } else if (measure >= 75 && measure < 103) {
                    level = 2;
                } else if (measure >= 103) {
                    level = 3;
                }
            }

            //Determino il livello di gravit� per le rilevazioni di tipo "pioggia"
            if (type == 'R') {
                if (measure >= 15 && measure < 30) {
                    level = 1;
                } else if (measure >= 30 && measure < 50) {
                    level = 2;
                } else if (measure >= 50) {
                    level = 3;
                }
            }

            //Determino il livello di gravit� per le rilevazioni di tipo "neve"
            if (type == 'S') {
                if (measure >= 1.5 && measure < 2) {
                    level = 1;
                } else if (measure >= 2 && measure < 3) {
                    level = 2;
                } else if (measure >= 3) {
                    level = 3;
                }
            }

            //Determino il livello di gravit� per le rilevazioni di tipo "terremoto"
            if (type == 'E') {
                if (measure >= 4 && measure < 4.7) {
                    level = 1;
                } else if (measure >= 4.7 && measure < 5.4) {
                    level = 2;
                } else if (measure >= 5.4) {
                    level = 3;
                }
            }

            //Converto l'orario della rilevazione in fascia oraria per verificare l'effettivo accadimento di un'allerta corrente
            if (timeDet.startsWith("00") || timeDet.startsWith("01") || timeDet.startsWith("02") || timeDet.startsWith("03")) {
                timeSlotDet = 1;
            } else if (timeDet.startsWith("04") || timeDet.startsWith("05") || timeDet.startsWith("06") || timeDet.startsWith("07")) {
                timeSlotDet = 2;
            } else if (timeDet.startsWith("08") || timeDet.startsWith("09") || timeDet.startsWith("10") || timeDet.startsWith("11")) {
                timeSlotDet = 3;
            } else if (timeDet.startsWith("12") || timeDet.startsWith("13") || timeDet.startsWith("14") || timeDet.startsWith("15")) {
                timeSlotDet = 4;
            } else if (timeDet.startsWith("16") || timeDet.startsWith("17") || timeDet.startsWith("18") || timeDet.startsWith("19")) {
                timeSlotDet = 5;
            } else if (timeDet.startsWith("20") || timeDet.startsWith("21") || timeDet.startsWith("22") || timeDet.startsWith("23")) {
                timeSlotDet = 6;
            }

            try {
                //Verifico l'effettivo accadimento di un'allerta corrente
                stubSystem.sendUpdate(cap, dateDet, timeSlotDet, level, type);
            } catch (RemoteException e) {
                System.err.println(id + " Error: StubCentralSystem for sendUpdate method (" + e.toString() + ")");
                // e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance();

            // Trasformo la rilevazione in previsione futura, sostituendo la fascia oraria corrente con una casuale delle successive 24h
            int currentH = 0;
            currentH = calendar.get(Calendar.HOUR_OF_DAY);

            //Prendo Ora da Calendar
            String dateF;
            short timeSlotF = 0;
            Random r = new Random();
            int forecastH;
            forecastH = r.nextInt(6); //Genero 6 fasce orarie
            forecastH = forecastH * 4;

            if (forecastH == 0) {
                timeSlotF = 1;
            } else if (forecastH == 4) {
                timeSlotF = 2;
            } else if (forecastH == 8) {
                timeSlotF = 3;
            } else if (forecastH == 12) {
                timeSlotF = 4;
            } else if (forecastH == 16) {
                timeSlotF = 5;
            } else if (forecastH == 20) {
                timeSlotF = 6;
            }

            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy"); //Format italiano e generazione giorno successivo

            // generate today date
            Date date = calendar.getTime();

            if (forecastH <= currentH) {
                // add one day
                calendar.add(Calendar.DATE, 1);

                // generate tomorrow date
                Date datePlusOne = calendar.getTime();

                dateF = dateFormat.format(datePlusOne);
            } else {
                dateF = dateFormat.format(date);
            }

            //Se la previsione non comporta un rischio per gli utenti, allora non vi alcun bisogno di generare una nuova allerta, bisogna però controllare che non indichi un'allerta rientrata.
            if (level == 0) {
                try {
                    //Verifico le allerte rientrate, ma non quelle per la fascia sucessiva
                    if (timeSlotF != currentH / 4 + 2)
                        stubSystem.removeAlert(cap, dateF, timeSlotF, type);
                } catch (RemoteException e) {
                    System.err.println(id + " Error: StubCentralSystem for removeAlert method (" + e.toString() + ")");
                    //e.printStackTrace();
                }
            } else { //Genero una nuova allerta se la rilevazione ha un livello di gravità > 0 e la salvo nella lista di allerte
                boolean valid = false;
                try {
                    Allerta alert = new Allerta(stubSystem.getCounterAndIncrement(), cap, dateF, timeSlotF, level, type, valid);
                    alertList.add(alert);
                } catch (RemoteException e) {
                    System.err.println(id + " Error: StubCentralSystem for getCounterAndIncrement method (" + e.toString() + ")");
                    //e.printStackTrace();
                }
            }
        }
        detection.clear();
    }

    private void filter() {
        takeDetections();
        generateForecasts();
    }

    public void stopAppPrevisioni() {
        try {
            executorService.shutdown();
            Registry registry = LocateRegistry.getRegistry("localhost");
            registry.unbind(id);
        } catch (RemoteException | NotBoundException e) {
            System.err.println(id + " Error: cannot stopping " + id + " ExecutorService\"); (" + e.toString() + ")");
            // e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Allerta> askAlert() {
        //creo copia perchè cancello l'originale
        ArrayList<Allerta> copy = new ArrayList<Allerta>();
        copy.addAll(alertList);
        alertList.clear();
        return copy;
    }

    public ArrayList<Integer> getCapList() {
        return capList;
    }

    public void setCapList(ArrayList<Integer> capList) {
        this.capList = capList;
    }

    public String getId() {
        return id;
    }

    @SuppressWarnings("unused")
    private void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getDetection() {
        return detection;
    }

    protected void setDetection(ArrayList<String> det) {
        this.detection.clear();
        this.detection = (ArrayList<String>) det.clone();
    }

    public ArrayList<Allerta> getAlertList() {
        return alertList;
    }

    protected void setAlertList(ArrayList<Allerta> aL) {
        this.alertList.clear();
        this.alertList.addAll(aL);
    }

    public ScheduledExecutorService getExecutorService() {
        return executorService;
    }

    public void setExecutorService(ScheduledExecutorService executorService) {
        this.executorService = executorService;
    }
}