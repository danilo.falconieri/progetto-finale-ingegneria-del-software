package appprevisione;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import protezionecivile.SistemaCentrale;
import serverRMI.ServerAppPrevisioni;
import serverRMI.ServerSisCentrale;
import tools.Utility;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class AppPrevisioniTest {

    Sensore s1;
    Sensore s2;
    Sensore s3;
    Sensore s4;
    SistemaCentrale sistemaCentrale;
    AppPrevisioni testAppPrevisioni;

    @Before
    public void setUp() {
        Utility.startRMIRegistry();
        sistemaCentrale = SistemaCentrale.getInstance();
        s1 = new Sensore(99999, 'W');
        s2 = new Sensore(99999, 'R');
        s3 = new Sensore(99999, 'S');
        s4 = new Sensore(99999, 'E');
        testAppPrevisioni = new AppPrevisioni("AppPrevisione.testAppPrevisioni", new ArrayList<>(Arrays.asList(99999)));
        sistemaCentrale.setAppList(new ArrayList<String>(Arrays.asList("AppPrevisione.testAppPrevisioni")));
    }

    @After
    public void tearDown() throws SQLException {		
        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            ServerSisCentrale stub = (ServerSisCentrale) registry.lookup("SistemaCentrale");
            stub.stopSystem();
            if (!testAppPrevisioni.getExecutorService().isShutdown()) {
                testAppPrevisioni.stopAppPrevisioni();
            }
            registry.unbind("Sensor.99999.W");
            registry.unbind("Sensor.99999.R");
            registry.unbind("Sensor.99999.S");
            registry.unbind("Sensor.99999.E");
        } catch (Exception e) {
            System.err.println("tearDown() Error: ");
            e.printStackTrace();
        }
        testAppPrevisioni = null;
        Utility.stopRMIRegistry();
        
        Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			connection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
			System.out.println("Connection with database established");
		} catch (ClassNotFoundException | SQLException e1) {
			System.err.println("ConnettoreDB Error: cannot connect to local database (" + e1.toString() + ")");
			// e.printStackTrace();
		}
		String queryDelete = "DELETE FROM alert WHERE cap=99999";
		Statement statement = connection.createStatement();
		statement.executeUpdate(queryDelete);
		statement.close();
		connection.close();
		connection = null;
    }

    @Test
    public void stopAppPrevisioni() {
        testAppPrevisioni.stopAppPrevisioni();
        assertTrue("AppPrevisione stoppata correttamente", testAppPrevisioni.getExecutorService().isShutdown());

        boolean isUnbind = false;
        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            ServerAppPrevisioni stubApp = (ServerAppPrevisioni) registry.lookup("AppPrevisione.testAppPrevisioni");
            stubApp.stopAppPrevisioni();
        } catch (Exception e) {
            isUnbind = true;
        }
        assertTrue("Unbind dell'AppPrevisione OK", isUnbind);
    }

    @Test
    public void askAlert() {
        testAppPrevisioni.stopAppPrevisioni();
        ArrayList<Allerta> testAlertList = new ArrayList<Allerta>(Arrays.asList(new Allerta(-1, 99999, "15-01-1997", (short) 1, (short) 3, 'E', false)));
        testAppPrevisioni.setAlertList(testAlertList);
        ArrayList<Allerta> resultTest = new ArrayList<Allerta>();
        resultTest = testAppPrevisioni.askAlert();
        boolean areEquals = true;
        for (int i = 0; i < resultTest.size(); ++i) {
            if (!testAlertList.get(i).toString().equals(resultTest.get(i).toString())) {
                areEquals = false;
            }
        }
        assertTrue("askAllerte funziona correttamente", areEquals);
    }

    @Test
    public void generateForecasts() {
        testAppPrevisioni.stopAppPrevisioni();
        testAppPrevisioni.getAlertList().clear();
        ArrayList<String> testDetection = new ArrayList<String>(Arrays.asList("99999;W;12-03-2020;19:30:00;49", "99999;W;12-03-2020;19:30:00;55", "99999;W;12-03-2020;19:30:00;80", "99999;W;12-03-2020;19:30:00;105", "99999;R;12-03-2020;19:30:00;10", "99999;R;12-03-2020;19:30:00;20", "99999;R;12-03-2020;19:30:00;40", "99999;R;12-03-2020;19:30:00;60", "99999;S;12-03-2020;19:30:00;1", "99999;S;12-03-2020;19:30:00;1.8", "99999;S;12-03-2020;19:30:00;2.5", "99999;S;12-03-2020;19:30:00;3", "99999;E;12-03-2020;19:30:00;3", "99999;E;12-03-2020;19:30:00;4", "99999;E;12-03-2020;19:30:00;5", "99999;E;12-03-2020;19:30:00;5.4"));
        testAppPrevisioni.setDetection(testDetection);
        testAppPrevisioni.generateForecasts();
        boolean found = true;

        //Controllo che in alertList vi siano effettivamente le allerte generate con i giusti valori in base alle rilevazioni ricevute
        for (int i = 0; i < testAppPrevisioni.getAlertList().size(); ++i) {
            if (testAppPrevisioni.getAlertList().get(i).getCap() != 99999) {
                found = false;
            }
        }
        if (!testAppPrevisioni.getAlertList().get(0).toString().endsWith("1;W;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(3).toString().endsWith("1;R;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(6).toString().endsWith("1;S;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(9).toString().endsWith("1;E;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(1).toString().endsWith("2;W;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(4).toString().endsWith("2;R;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(7).toString().endsWith("2;S;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(10).toString().endsWith("2;E;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(2).toString().endsWith("3;W;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(5).toString().endsWith("3;R;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(8).toString().endsWith("3;S;false")) {
            found = false;
        } else if (!testAppPrevisioni.getAlertList().get(11).toString().endsWith("3;E;false")) {
            found = false;
        }

        assertTrue("generateForecasts funziona correttamente", found);

        //Non vado a testare le funzionalit� di sendUpdate() e removeAlert() in quanto vengono gi� testate nel ConnettoreDBTest,
        //nei relativi test testQueryConfirmAlert e testQueryDeleteAlert.
    }
}




















