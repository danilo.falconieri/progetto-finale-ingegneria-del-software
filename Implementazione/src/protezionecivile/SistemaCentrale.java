package protezionecivile;

import serverRMI.ServerAppPrevisioni;
import serverRMI.ServerSisCentrale;
import appprevisione.Allerta;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SistemaCentrale implements ServerSisCentrale, Runnable {

    private static SistemaCentrale instance = null;

    // viene inizializzata la connessione al database da usare in mutua esclusione
    private final AccessoDB connectionDB = new ConnettoreDB();

    private static volatile int COUNTER = 0;
    private ArrayList<String> appList = new ArrayList<>();
    private ArrayList<ArrayList<Integer>> capList = new ArrayList<>();
    private ScheduledExecutorService executorService;
    private Map<Integer, ArrayList<String>> cacheNotices = new HashMap<Integer, ArrayList<String>>();
    private Map<Integer, ArrayList<String>> cacheDefaultAlert = new HashMap<Integer, ArrayList<String>>();

    private SistemaCentrale() {
        // avvio RMIServerSistemaCentrale
        try {
            ServerSisCentrale stub = (ServerSisCentrale) UnicastRemoteObject.exportObject(this, 0);
            Registry registry = LocateRegistry.getRegistry("localhost");
            registry.bind("SistemaCentrale", stub);

            System.out.println("ServerSistemaCentrale ready");
        } catch (RemoteException | AlreadyBoundException e) {
            System.err.println("CentralSystem Error: cannot bind CentralSystem to rmiregistry (" + e.toString() + ")");
            // e.printStackTrace();
        }

        // viene impostato il valore della variabile COUNTER, serve per la generazione
        // di id univoci per le allerte
        setCounter();

        // creo uno ScheduledExecutorService che lavora come un Timer. Inizia con un
        // Delay pari a millisToNextFourHour() e ripete this.run() ogni 4 ore
        executorService = Executors.newSingleThreadScheduledExecutor();

        // REAL:
        executorService.scheduleAtFixedRate(this, millisToNextFourHour(), 14400000, TimeUnit.MILLISECONDS);
    }

    @Override
    public void run() {
        cacheNotices.clear();
        cacheDefaultAlert.clear();

        // per ogni appPrevisione prelevo la lista di allerte e la inserisco nel
        // database
        for (String appName : appList) {
            try {
                Registry reg = LocateRegistry.getRegistry("localhost");
                ServerAppPrevisioni stub = (ServerAppPrevisioni) reg.lookup(appName);
                updateDatabase(stub.askAlert());
            } catch (RemoteException | NotBoundException e) {
                System.err
                        .println("CentralSystem Error: cannot connection to StubAppPrevisione (" + e.toString() + ")");
                // e.printStackTrace();
            }
        }
        System.out.println("SistemaCentrale IS RUNNING()");
    }

    // Metodo che ritorna la differenza in millisecondi tra l'ora attuale (now) e
    // l'ora multipla di 4 sucessiva (next)
    private long millisToNextFourHour() {
        LocalTime now = LocalTime.now().truncatedTo(ChronoUnit.MILLIS);
        LocalTime next;

        int hour = now.getHour();
        // fascia 00:00:00 - 03:59:59
        if (hour < 4)
            next = LocalTime.of(4, 0, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            // fascia 04:00:00 - 07:59:59
        else if (hour < 8)
            next = LocalTime.of(8, 0, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            // fascia 08:00:00 - 11:59:59
        else if (hour < 12)
            next = LocalTime.of(12, 0, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            // fascia 12:00:00 - 15:59:59
        else if (hour < 16)
            next = LocalTime.of(16, 0, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            // fascia 16:00:00 - 19:59:59
        else if (hour < 20)
            next = LocalTime.of(20, 0, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            // fascia 20:00:00 - 23:59:59
        else {
            // siccome in questo caso per fare la differenza con mezzanotte ho bisogno di
            // sapere anche il giorno, uso i LocalDateTime (perch� devo fare la differenza
            // con la mezzanotte del giorno seguente)
            LocalDateTime nowDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);
            LocalDateTime nextDateTime = null;
            try {
            	nextDateTime = LocalDateTime.of(nowDateTime.getYear(), nowDateTime.getMonth(), nowDateTime.getDayOfMonth() + 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            } catch (Exception e) {
            	try {
            		nextDateTime = LocalDateTime.of(nowDateTime.getYear(), nowDateTime.getMonth().plus(1), 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);	
            	} catch (Exception e1) {
            		nextDateTime = LocalDateTime.of(nowDateTime.getYear() + 1, Month.JANUARY, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);	
            	}
            }
            Duration timeElapsed = Duration.between(nowDateTime, nextDateTime);
            return timeElapsed.toMillis();
        }

        Duration timeElapsed = Duration.between(now, next);
        return timeElapsed.toMillis();
    }

    // Restituisce una lista di allerte filtrate per cap, fascia oraria e/o
    // tipologia
    public synchronized ArrayList<String> listAlarm(Integer cap, Short timeSlot, Character type, String date) {
        ArrayList<String> out = null;
        boolean err = false;

        synchronized (connectionDB) {
            checkConnectionDB();
            try {
                out = new ArrayList<>(connectionDB.getData(cap, timeSlot, type, date));
            } catch (SQLException e) {
                System.err.println("CentralSystem Error: connectionDB for getData method (" + e.toString() + ")");
                // e.printStackTrace();
                err = true;
            } finally {
                closeConnectionDB();
            }
        }

        if (err)
            return null;
        return out;
    }

    // Restituisce una lista delle allerte pi� gravi, per il giorno corrente,
    // ordinate per cap e fascia oraria
    public synchronized ArrayList<String> listSeriousAlarm() {
        ArrayList<String> out = null;
        boolean err = false;

        synchronized (connectionDB) {
            checkConnectionDB();
            try {
                out = new ArrayList<>(connectionDB.getData());
            } catch (SQLException e) {
                System.err.println("CentralSystem Error: connectionDB for getData method (" + e.toString() + ")");
                // e.printStackTrace();
                err = true;
            } finally {
                closeConnectionDB();
            }
        }

        if (err)
            return null;
        return out;
    }

    // Conferma l'accadimento di un'allerta, i parametri in ingresso corrispondono
    // alla rilevazione si un sensore
    public synchronized void sendUpdate(int cap, String date, short timeSlot, short level, char type) {
        synchronized (connectionDB) {
            checkConnectionDB();
            try {
                connectionDB.confirmAlert(cap, date, timeSlot, level, type);
            } catch (SQLException e) {
                System.err.println("CentralSystem Error: connectionDB for confirmAlert method (" + e.toString() + ")");
                // e.printStackTrace();
            } finally {
                closeConnectionDB();
            }
        }
    }

    // Rimuove dal database un'allerta corrispondente ai parametri dati in ingresso
    public synchronized void removeAlert(int cap, String date, short timeSlot, char type) {
        synchronized (connectionDB) {
            checkConnectionDB();
            try {
                connectionDB.deleteAlert(cap, date, timeSlot, type);
            } catch (SQLException e) {
                System.err.println("Error: cannot run method deleteAlert of ConnettoreDB");
                // e.printStackTrace();
            } finally {
                closeConnectionDB();
            }
        }
    }

    // Restituisce una lista di allerte valide per la fascia oraria successiva, per
    // il cap richiesto
    public synchronized ArrayList<String> askNotice(int cap) {
        if (cacheNotices == null)
            cacheNotices = new HashMap<Integer, ArrayList<String>>();

        boolean err = false;
        ArrayList<String> out = cacheNotices.get(cap);
        if (out == null) {
            synchronized (connectionDB) {
                checkConnectionDB();
                try {
                    out = connectionDB.createNotice(cap);
                } catch (SQLException e) {
                    System.err.println("CentralSystem Error: connectionDB for createNotice method (" + e.toString() + ")");
                    // e.printStackTrace();
                    err = true;
                } finally {
                    closeConnectionDB();
                }
            }

            if (err)
                return null;
            // saving in cache
            cacheNotices.put(cap, out);
        }
        return out;
    }

    // Quando l'utente apre la connessione col SistemaCentrale, riceve una lista di
    // allerte relative al suo cap per le successive 24 ore
    public synchronized ArrayList<String> getDefaultAlert(int cap) {
        if (cacheDefaultAlert == null)
            cacheDefaultAlert = new HashMap<Integer, ArrayList<String>>();

        boolean err = false;
        ArrayList<String> out = cacheDefaultAlert.get(cap);
        if (out == null) {
            synchronized (connectionDB) {
                checkConnectionDB();
                try {
                    out = connectionDB.getDefaultAlert(cap);
                } catch (SQLException e) {
                    System.err
                            .println("CentralSystem Error: connectionDB for openConnection method (" + e.toString() + ")");
                    // e.printStackTrace();
                    err = true;
                } finally {
                    closeConnectionDB();
                }
            }
            if (err)
                return null;
            // saving in cache
            cacheDefaultAlert.put(cap, out);
        }
        return out;
    }

    // Assegna al contatore il primo valore disponibile
    private synchronized void setCounter() {
        synchronized (connectionDB) {
            checkConnectionDB();
            try {
                COUNTER = connectionDB.getIndex();
            } catch (SQLException e) {
                System.err.println("CentralSystem Error: connectionDB for getIndex method (" + e.toString() + ")");
                // e.printStackTrace();
            } finally {
                closeConnectionDB();
            }
        }
    }

    // Ritorna alle AppPrevisioni il valore da assegnare come id ad una nuova
    // allerta
    public synchronized int getCounterAndIncrement() {
        synchronized (this) {
            return COUNTER++;
        }
    }

    // Aggiorna il database inserendo le nuove allerte o modificando i dati di
    // quelle gi� presenti
    private synchronized void updateDatabase(ArrayList<Allerta> list) {
        synchronized (connectionDB) {
            checkConnectionDB();
            try {
                connectionDB.updateDatabase(list);
            } catch (SQLException e) {
                System.err.println("CentralSystem Error: connectionDB for updateDatabase method (" + e.toString() + ")");
                // e.printStackTrace();
            } finally {
                closeConnectionDB();
            }
        }

    }

    // Verifica che un cap sia presente nel sistema
    @SuppressWarnings("unused")
    public boolean checkInCapList(Integer cap) {
        int iterator = 0;
        for (String appName : appList) {
            if (capList.get(iterator).contains(cap)) {
                return true;
            }
            iterator++;
        }
        return false;
    }

    // Restituisce la lista di tutti cap, divisi per regione, all'utente
    public ArrayList<String> getCapList() {
        ArrayList<String> list = new ArrayList<String>();
        String out;
        for (int i = 0; i < appList.size(); i++) {
            out = "";
            if (appList.get(i).length() > 14 && appList.get(i).substring(0, 14).equals("AppPrevisione."))
                out = out + appList.get(i).subSequence(14, appList.get(i).length()) + ": " + capList.get(i);
            else
                out = out + appList.get(i) + ": " + capList.get(i);
            list.add(out);
        }
        Collections.sort(list);
        return list;
    }

    // Per ogni AppPrevisioni prelevo la lista dei cap e l'aggiungo alla matrice
    // capList
    private void setCapList() {
        for (String appName : appList) {
            try {
                Registry registry = LocateRegistry.getRegistry("localhost");
                ServerAppPrevisioni stubApp = (ServerAppPrevisioni) registry.lookup(appName);
                capList.add(stubApp.getCapList());
            } catch (RemoteException | NotBoundException e) {
                System.err.println("CentralSystem Error: stubAppPrevisioni for getCapList method (" + e.toString() + ")");
                // e.printStackTrace();
            }
        }
    }

    // Ferma l'esecuzione del SistemaCentrale e ne fa l'unbind dal registro, oltre a
    // chiudere la connessione col database
    public void stopSystem() {
        try {
            executorService.shutdown();
            Registry registry = LocateRegistry.getRegistry("localhost");
            registry.unbind("SistemaCentrale");
            if (!connectionDB.isClose())
                connectionDB.closeConnection();
            instance = null;
        } catch (RemoteException | NotBoundException | SQLException e) {
            System.err.println(
                    "CentralSystem Error: cannot stopping CentralSystem ExecutorService (" + e.toString() + ")");
            // e.printStackTrace();
        }
    }

    // Assegna alla variabile appList l'elenco di tutte le AppPrevisioni esistenti
    // da eseguire dopo che le AppPrevisioni abbiano fatto la bind. Se cos� non
    // fosse va in errore il sisteam centrale
    public void setAppList(ArrayList<String> app) {
        appList.addAll(app);
        setCapList();
        // all'avvio aggiorno il db con i nuovi dati
        run();
    }

    // Rimuove un AppPrevisioni dalla variabile appList
    public void removeInAppList(String app) {
        if (!appList.contains(app))
            return;
        else
            appList.remove(appList.indexOf(app));
    }

    // metodo che controlla la conessione al database
    private void checkConnectionDB() {
        try {
            while (connectionDB.isClose())
                connectionDB.openConnection();
        } catch (SQLException ignore) {
        }
    }

    private void closeConnectionDB() {
        try {
            connectionDB.closeConnection();
        } catch (SQLException ignore) {
        }

    }

    public static SistemaCentrale getInstance() {
        if (instance == null)
            instance = new SistemaCentrale();

        return instance;
    }
}
