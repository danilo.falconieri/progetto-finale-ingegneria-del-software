package protezionecivile;

import appprevisione.Allerta;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// Tutti i test sono eseguiti per un cap inesistente nella realt� (99999) e ogni allerta inserita nel database
// ha un id negativo o pari a zero. In questo modo si � certi di non modificare i dati gi� presenti nel database.

public class ConnettoreDBTest {

	private AccessoDB connector;
	private static final int ID = 0;
	private Connection connection = null;

	@Before
	// Inserisce nel database l'allerta con cui verranno testati alcuni dei metodi
	// del database, oltre a cancellare dal database allerte aventi id<=0
	public void setUp() throws Exception {
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			connection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
			System.out.println("Connection with database established");
			initialiseDB();
		} catch (ClassNotFoundException | SQLException e1) {
			System.err.println("ConnettoreDB Error: cannot connect to local database (" + e1.toString() + ")");
			// e.printStackTrace();
		}

		String queryDelete = "DELETE FROM alert WHERE cap=99999";
		Statement statement = connection.createStatement();
		statement.executeUpdate(queryDelete);
		Allerta alert = new Allerta(ID, 99999, "05-03-2019", (short) 3, (short) 2, 'W', false);
		
		String date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse("05-03-2019"));
		} catch (ParseException e) {
			System.err.println("Error parsing string");
		}
		
		String queryInsert = "INSERT INTO alert VALUES ('" + alert.getId() + "','" + alert.getCap() + "','"
				+ date + "','" + (int) alert.getTimeSlot() + "','" + (int) alert.getLevel() + "','"
				+ alert.getType() + "','" + Boolean.toString(alert.getValid()) + "','Strong wind')";
		statement = connection.createStatement();
		statement.executeUpdate(queryInsert);
		statement.close();
		connection.close();
		connection = null;
		connector = new ConnettoreDB();
	}

	@After
	// Chiude le connessioni col database
	public void tearDown() throws Exception {
		connector.closeConnection();
		connector = null;
		
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			connection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
		} catch (ClassNotFoundException | SQLException e1) {
			System.err.println("ConnettoreDB Error: cannot connect to local database (" + e1.toString() + ")");
			// e.printStackTrace();
		}
		String queryDelete = "DELETE FROM alert WHERE cap=99999";
		Statement statement = connection.createStatement();
		statement.executeUpdate(queryDelete);
		statement.close();
		connection.close();
		connection = null;
		
		assertNull(connector);
		assertNull(connection);
	}

	@Test
	// Verifica il metodo per filtrare le allerte del database per cap, date, time
	// slot e/o type
	public void testQueryGetAlarm() throws SQLException {
		Integer cap = 99999;
		Short timeSlot = 3;
		Character type = 'W';
		ArrayList<String> list = connector.getData(cap, timeSlot, type, "05-03-2019");
		if (!list.isEmpty()) {
			for (String temp : list) {
				int countSemicolon = 0;
				String verifyCap = "";
				String verifyTimeSlot = "";
				String verifyType = "";
				for (int i = 0; i < temp.length(); i++) {
					if (temp.charAt(i) == ';') {
						countSemicolon++;
					} else {
						switch (countSemicolon) {
						case 1:
							verifyCap += temp.charAt(i);
							break;
						case 3:
							verifyTimeSlot += temp.charAt(i);
							break;
						case 5:
							verifyType += temp.charAt(i);
							break;
						}
					}
				}
				assertEquals("Correct cap", Integer.toString(cap), verifyCap);
				assertEquals("Correct time slot", Short.toString(timeSlot), verifyTimeSlot);
				assertEquals("Correct type", verifyType, "W");
			}
		} else {
			assertEquals("Empty list", list.size(), 0);
		}
	}

	@Test
	// Verifica il metodo per estrarre dal database gli allarmi in evidenza
	public void testQueryGetSeriousAlarm() throws SQLException {
		Date date = new Date();
		long timeT = date.getTime();
		Timestamp time = new Timestamp(timeT);
		String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(time);
		Allerta alert1 = new Allerta(-1, 99999, currentDate, (short) 3, (short) 3, 'E', false);
		Allerta alert2 = new Allerta(-2, 99999, currentDate, (short) 6, (short) 2, 'W', false);
		Allerta alert3 = new Allerta(-3, 99999, currentDate, (short) 1, (short) 2, 'W', false);
		Allerta alert4 = new Allerta(-4, 99999, currentDate, (short) 2, (short) 1, 'W', false);
		Allerta alert5 = new Allerta(-5, 99999, currentDate, (short) 2, (short) 3, 'W', false);
		Allerta alert6 = new Allerta(-6, 99999, currentDate, (short) 4, (short) 2, 'W', false);
		Allerta alert7 = new Allerta(-7, 99999, currentDate, (short) 1, (short) 2, 'R', false);
		Allerta alert8 = new Allerta(-8, 99999, currentDate, (short) 5, (short) 3, 'S', false);
		Allerta alert9 = new Allerta(-9, 99999, currentDate, (short) 5, (short) 3, 'W', false);
		ArrayList<Allerta> listAlert = new ArrayList<Allerta>();
		listAlert.add(alert1);
		listAlert.add(alert2);
		listAlert.add(alert3);
		listAlert.add(alert4);
		listAlert.add(alert5);
		listAlert.add(alert6);
		listAlert.add(alert7);
		listAlert.add(alert8);
		listAlert.add(alert9);
		connector.updateDatabase(listAlert);
		ArrayList<String> testResult = new ArrayList<String>();
		testResult = connector.getData();
		ArrayList<String> result = new ArrayList<String>();
		for (String temp : testResult) {
			if (temp.contains("99999")) {
				result.add(temp);
			}
		}
		int countTimeSlotOne = 0;
		int countTimeSlotTwo = 0;
		int countTimeSlotThree = 0;
		int countTimeSlotFour = 0;
		int countTimeSlotFive = 0;
		int countTimeSlotSix = 0;
		for (String temp : result) {
			switch (temp.charAt(17)) {
			case '1':
				countTimeSlotOne++;
				break;
			case '2':
				countTimeSlotTwo++;
				break;
			case '3':
				countTimeSlotThree++;
				break;
			case '4':
				countTimeSlotFour++;
				break;
			case '5':
				countTimeSlotFive++;
				break;
			case '6':
				countTimeSlotSix++;
				break;
			}
		}
		assertEquals("Correct number of alerts", result.size(), 8);
		assertEquals("Correct number of alerts for time slot 1", 2, countTimeSlotOne);
		assertEquals("Correct number of alerts for time slot 2", 1, countTimeSlotTwo);
		assertEquals("Correct number of alerts for time slot 3", 1, countTimeSlotThree);
		assertEquals("Correct number of alerts for time slot 4", 1, countTimeSlotFour);
		assertEquals("Correct number of alerts for time slot 5", 2, countTimeSlotFive);
		assertEquals("Correct number of alerts for time slot 6", 1, countTimeSlotSix);
	}

	@Test
	// Verifica il metodo per confermare un'allerta presente nel database
	public void testQueryConfirmAlert() throws SQLException {
		connector.confirmAlert(99999, "05-03-2019", (short) 3, (short) 2, 'W');
		ArrayList<String> list = connector.getData(99999, (short) 3, 'W', "05-03-2019");
		for (String temp : list) {
			if (temp.contains("05-03-2019") && Integer.valueOf(temp.charAt(21)) == 2) {
				assertEquals("Alert confirm", temp.contains("true"), true);
			}
		}
	}

	@Test
	// Verifica il metodo per eliminare un'allerta dal database
	public void testQueryDeleteAlert() throws SQLException {
		connector.deleteAlert(99999, "05-03-2019", (short) 3, 'W');
		ArrayList<String> list = connector.getData(99999, (short) 3, 'W', "05-03-2019");
		assertEquals("Alert cleared", list.size(), 0);
	}

	@Test
	// Verifica che un'allerta gi� presente nel database venga aggiornata di grado
	public void testQueryUpdateDatabase() throws SQLException {
		Allerta alert = new Allerta(ID, 99999, "05-03-2019", (short) 3, (short) 3, 'W', false);
		ArrayList<Allerta> listAlert = new ArrayList<Allerta>();
		listAlert.add(alert);
		connector.updateDatabase(listAlert);
		ArrayList<String> rs = connector.getData(99999, (short) 3, 'W', "05-03-2019");
		String sel[] = rs.get(0).split(";");

		assertEquals("Correct ID", Integer.toString(ID), sel[0]);
		assertEquals("Correct cap", Integer.toString(99999), sel[1]);
		assertEquals("Correct date", "05-03-2019", sel[2]);
		assertEquals("Correct time slot", Integer.toString(3), sel[3]);
		assertEquals("Correct level", Integer.toString(3), sel[4]);
		assertEquals("Correct type", "W", sel[5]);
		assertEquals("Correct occurence", "false", sel[6]);
		assertEquals("Correct descritpion", "Heavy wind", sel[7]);
	}

	@Test
	// Verifica che un'allerta non presente nel database venga inserita
	public void testQueryInsertDatabase() throws SQLException {
		connector.deleteAlert(99999, "05-03-2019", (short) 3, 'W');
		Allerta alert = new Allerta(ID, 99999, "05-03-2019", (short) 3, (short) 2, 'W', false);
		ArrayList<Allerta> listAlert = new ArrayList<Allerta>();
		listAlert.add(alert);
		connector.updateDatabase(listAlert);
		ArrayList<String> rs = connector.getData(99999, (short) 3, 'W', "05-03-2019");
		String sel[] = rs.get(0).split(";");

		assertEquals("Correct ID", Integer.toString(ID), sel[0]);
		assertEquals("Correct cap", Integer.toString(99999), sel[1]);
		assertEquals("Correct date", "05-03-2019", sel[2]);
		assertEquals("Correct time slot", Integer.toString(3), sel[3]);
		assertEquals("Correct level", Integer.toString(2), sel[4]);
		assertEquals("Correct type", "W", sel[5]);
		assertEquals("Correct occurence", "false", sel[6]);
		assertEquals("Correct descritpion", "Strong wind", sel[7]);
	}

	@Test
	// Verifica che, ad inizio sessione, ad un utente venga mostrato l'elenco delle
	// allerte delle successive 24 ore per il suo cap
	public void testGetDefaultAlert() throws SQLException {
		Date date = new Date();
		long timeT = date.getTime();
		Timestamp time = new Timestamp(timeT);
		String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(time);
		String currentHour = new SimpleDateFormat("HH").format(time);
		int currentTimeSlot = 1 + Integer.valueOf(currentHour) / 4;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		String tomorrowDate = new SimpleDateFormat("dd-MM-yyyy").format(calendar.getTime());
		Allerta alert1 = new Allerta(-1, 99999, tomorrowDate, (short) 3, (short) 3, 'E', false);
		Allerta alert2 = new Allerta(-2, 99999, currentDate, (short) 6, (short) 2, 'W', false);
		Allerta alert3 = new Allerta(-3, 99999, tomorrowDate, (short) 1, (short) 2, 'W', false);
		Allerta alert4 = new Allerta(-4, 99999, currentDate, (short) 2, (short) 1, 'W', false);
		Allerta alert5 = new Allerta(-5, 99999, tomorrowDate, (short) 2, (short) 3, 'W', false);
		Allerta alert6 = new Allerta(-6, 99999, currentDate, (short) 4, (short) 2, 'W', false);
		Allerta alert7 = new Allerta(-7, 99999, currentDate, (short) 1, (short) 2, 'R', false);
		Allerta alert8 = new Allerta(-8, 99999, tomorrowDate, (short) 5, (short) 3, 'S', false);
		Allerta alert9 = new Allerta(-9, 99999, currentDate, (short) 5, (short) 3, 'W', false);
		ArrayList<Allerta> listAlert = new ArrayList<Allerta>();
		listAlert.add(alert1);
		listAlert.add(alert2);
		listAlert.add(alert3);
		listAlert.add(alert4);
		listAlert.add(alert5);
		listAlert.add(alert6);
		listAlert.add(alert7);
		listAlert.add(alert8);
		listAlert.add(alert9);
		connector.updateDatabase(listAlert);
		ArrayList<String> list = connector.getDefaultAlert(99999);
		boolean correct = true;
		for (String temp : list) {
			if (temp.contains(currentDate) && Integer.valueOf(temp.charAt(17)) <= currentTimeSlot) {
				correct = false;
			}
		}
		assertTrue(correct);
	}

	@Test
	// Verifica il metodo che crea le notifiche per l'utente
	public void testQueryCreateNotice() throws SQLException {
		Date date = new Date();
		long timeT = date.getTime();
		Timestamp time = new Timestamp(timeT);
		String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(time);
		String currentHour = new SimpleDateFormat("HH").format(time);
		int currentTimeSlot = 1 + Integer.valueOf(currentHour) / 4;

		Allerta alert1 = new Allerta(-2, 99999, currentDate, (short) 1, (short) 2, 'W', false);
		Allerta alert2 = new Allerta(-3, 99999, currentDate, (short) 2, (short) 2, 'W', false);
		Allerta alert3 = new Allerta(-4, 99999, currentDate, (short) 3, (short) 1, 'W', false);
		Allerta alert4 = new Allerta(-5, 99999, currentDate, (short) 4, (short) 3, 'W', false);
		Allerta alert5 = new Allerta(-6, 99999, currentDate, (short) 5, (short) 2, 'W', false);
		Allerta alert6 = new Allerta(-1, 99999, currentDate, (short) 6, (short) 3, 'E', false);
		ArrayList<Allerta> listAlert = new ArrayList<Allerta>();
		listAlert.add(alert1);
		listAlert.add(alert2);
		listAlert.add(alert3);
		listAlert.add(alert4);
		listAlert.add(alert5);
		listAlert.add(alert6);
		connector.updateDatabase(listAlert);
		ArrayList<String> list = new ArrayList<String>();
		list = connector.createNotice(99999);
		assertEquals("Correct number of alerts", 1, list.size());
		assertEquals("Correct time slot", Integer.toString(currentTimeSlot),Character.toString(list.get(0).charAt(17)));
	}

	@Test
	// Verifica il metodo che chiude la connessione col database. Dopo averlo
	// invocato, si prova a chiamare un altro metodo ConnettoreDB, se viene lanciata
	// un'eccezione significa che la connessione � stata chiusa
	public void testCloseConnection() throws SQLException {
		connector.closeConnection();
		try {
			connector.getIndex();
		} catch (SQLException e) {
			assertEquals("Connection closed", true, true);
		}
	}

	// Il metodo getIndex() del ConnettoreDB � gi� stato testato nella classe
	// SistemaCentraleTest all'interno di testContatore

	// Verifica che nel database sia presente la tabella "alert" e, nel caso non ci
	// fosse, la crea
	private void initialiseDB() throws SQLException {
		// check if there is the table
		DatabaseMetaData dbm = connection.getMetaData();
		ResultSet rs = dbm.getTables(null, null, "alert", null);

		// if not exist table alert, create alert table
		if (rs.next()) {
		} else {
			Statement st = connection.createStatement();
			st.executeUpdate(
					"CREATE TABLE alert(" + "id INT PRIMARY KEY," + "cap INT," + "date DATE," + "timeslot INT,"
							+ "level INT," + "type CHAR(1)," + "occur VARCHAR(45)," + "description VARCHAR(45)" + ");");
			System.out.println("created the table 'alert'");
		}
		
		rs.close();
	}

}
