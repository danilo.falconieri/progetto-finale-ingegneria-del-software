package protezionecivile;

import appprevisione.Allerta;

import java.sql.SQLException;
import java.util.ArrayList;

public interface AccessoDB {

    public ArrayList<String> getData(Integer cap, Short timeSlot, Character type, String date) throws SQLException;

    public ArrayList<String> getData() throws SQLException;

    public void confirmAlert(int cap, String date, short timeSlot, short level, char type) throws SQLException;

    public void deleteAlert(int cap, String date, short timeSlot, char type) throws SQLException;

    public void updateDatabase(ArrayList<Allerta> list) throws SQLException;

    public ArrayList<String> createNotice(int cap) throws SQLException;

    public ArrayList<String> getDefaultAlert(int cap) throws SQLException;

    public int getIndex() throws SQLException;

    public void closeConnection() throws SQLException;

    public void openConnection();

    public boolean isClose() throws SQLException;
}
