package protezionecivile;

import appprevisione.Allerta;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ConnettoreDB implements AccessoDB {

	private Connection connection;

	// Il costruttore stabilisce la connessione con il database
	public ConnettoreDB() {
		openConnection();
	}

	private void initialiseDB() throws SQLException {
		// check if there is the table
		ResultSet rs = connection.getMetaData().getTables(null, null, "alert", null);

		// if not exist table alert, create alert table
		if (!rs.next()) {
			Statement st = connection.createStatement();
			st.executeUpdate("CREATE TABLE alert(" + "id INT PRIMARY KEY," + "cap INT," + "date DATE," + "timeslot INT,"
					+ "level INT," + "type CHAR(1)," + "occur VARCHAR(45)," + "description VARCHAR(45)" + ");");
			System.out.println("created the table 'alert'");
		}

		rs.close();
	}

	// Estrae dal database le tuple filtrate per cap, time slot e/o type
	public ArrayList<String> getData(Integer cap, Short timeSlot, Character type, String searchDate) throws SQLException {
		ArrayList<String> list = new ArrayList<String>();
		Statement statement = connection.createStatement();
		ResultSet rs;
		
		String date = null;
		if (searchDate != null) {
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse(searchDate));
			} catch (ParseException e) {
				System.err.println("Error parsing string");
			}
		}
		
		if (cap != null && timeSlot == null && type == null && date == null) {
			String queryCap = "SELECT * FROM alert WHERE cap=" + cap + " ORDER BY date DESC,timeslot,id";
			rs = statement.executeQuery(queryCap);
		} else if (cap != null && timeSlot == null && type == null && date != null) {
			String queryCapDate = "SELECT * FROM alert WHERE cap=" + cap + " AND date='" + date
					+ "' ORDER BY timeslot,id";
			rs = statement.executeQuery(queryCapDate);
		} else if (cap != null && timeSlot != null && type == null && date == null) {
			String queryCapTime = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot=" + timeSlot
					+ " ORDER BY date DESC,id";
			rs = statement.executeQuery(queryCapTime);
		} else if (cap != null && timeSlot != null && type == null && date != null) {
			String queryCapTimeDate = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot=" + timeSlot
					+ " AND date='" + date + "' ORDER BY id";
			rs = statement.executeQuery(queryCapTimeDate);
		} else if (cap != null && timeSlot == null && type != null && date == null) {
			String queryCapType = "SELECT * FROM alert WHERE cap=" + cap + " AND type='" + type
					+ "' ORDER BY date DESC,timeslot,id";
			rs = statement.executeQuery(queryCapType);
		} else if (cap != null && timeSlot == null && type != null && date != null) {
			String queryCapTypeDate = "SELECT * FROM alert WHERE cap=" + cap + " AND type='" + type + "' AND date='"
					+ date + "' ORDER BY timeslot,id";
			rs = statement.executeQuery(queryCapTypeDate);
		} else if (cap == null && timeSlot != null && type == null && date == null) {
			String queryTime = "SELECT * FROM alert WHERE timeslot=" + timeSlot + " ORDER BY date DESC,cap,id";
			rs = statement.executeQuery(queryTime);
		} else if (cap == null && timeSlot != null && type == null && date != null) {
			String queryTimeDate = "SELECT * FROM alert WHERE timeslot=" + timeSlot + " AND date='" + date
					+ "' ORDER BY cap,id";
			rs = statement.executeQuery(queryTimeDate);
		} else if (cap == null && timeSlot != null && type != null && date == null) {
			String queryTimeType = "SELECT * FROM alert WHERE timeslot=" + timeSlot + " AND type='" + type
					+ "' ORDER BY date DESC,cap,id";
			rs = statement.executeQuery(queryTimeType);
		} else if (cap == null && timeSlot != null && type != null && date != null) {
			String queryTimeTypeDate = "SELECT * FROM alert WHERE timeslot=" + timeSlot + " AND type='" + type
					+ "' AND date='" + date + "' ORDER BY cap,id";
			rs = statement.executeQuery(queryTimeTypeDate);
		} else if (cap == null && timeSlot == null && type != null && date == null) {
			String queryType = "SELECT * FROM alert WHERE type='" + type + "' ORDER BY date DESC,cap,timeslot,id";
			rs = statement.executeQuery(queryType);
		} else if (cap == null && timeSlot == null && type != null && date != null) {
			String queryTypeDate = "SELECT * FROM alert WHERE type='" + type + "' AND date='" + date
					+ "' ORDER BY cap,timeslot,id";
			rs = statement.executeQuery(queryTypeDate);
		} else if (cap == null && timeSlot == null && type == null && date == null) {
			String queryNull = "SELECT * FROM alert ORDER BY date DESC,timeslot,cap,id";
			rs = statement.executeQuery(queryNull);
		} else if (cap == null && timeSlot == null && type == null && date != null) {
			String queryDate = "SELECT * FROM alert WHERE date='" + date + "' ORDER BY cap,timeslot,id";
			rs = statement.executeQuery(queryDate);
		} else if (cap != null && timeSlot != null && type != null && date == null) {
			String queryCapTimeType = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot=" + timeSlot
					+ " AND type='" + type + "' ORDER BY date DESC,id";
			rs = statement.executeQuery(queryCapTimeType);
		} else {
			String queryAll = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot=" + timeSlot + " AND type='"
					+ type + "' AND date='" + date + "' ORDER BY id";
			rs = statement.executeQuery(queryAll);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
		while (rs.next()) {			
			String printDate = null;
			try {
				Date tempDate = sdf.parse(rs.getString("date"));
				printDate = new SimpleDateFormat("dd-MM-yyyy").format(tempDate);
			} catch (ParseException e) {
				System.err.println("Error parsing and formatting string");
			}
			
			list.add(rs.getString("id") + ";" + rs.getString("cap") + ";" + printDate + ";" + rs.getString("timeslot") + ";" +
					  rs.getString("level") + ";" + rs.getString("type") + ";" +
					  rs.getString("occur") + ";" + rs.getString("description"));
			 
		}

		rs.close();
		statement.close();

		return list;
	}

	// Estrae dal database le tuple filtrate per cap e timeslot che hanno grado di
	// allerta massimo
	public ArrayList<String> getData() throws SQLException {
		ArrayList<String> list = new ArrayList<String>();
		
		Date date = new Date();
		long timeT = date.getTime();
		Timestamp time = new Timestamp(timeT);
		
		String currentDate =  new SimpleDateFormat("yyyy-MM-dd").format(time);

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM alert a1 WHERE a1.date='" + currentDate + "' AND a1.level="
				+ "(SELECT MAX(a2.level) FROM alert a2 WHERE a1.timeslot=a2.timeslot AND a1.cap=a2.cap"
				+ " AND a1.date=a2.date) GROUP BY a1.cap, a1.timeslot, a1.type";
		ResultSet rs = statement.executeQuery(query);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		while (rs.next()) {
			String printDate = null;
			try {
				Date tempDate = sdf.parse(rs.getString("date"));
				printDate = new SimpleDateFormat("dd-MM-yyyy").format(tempDate);
			} catch (ParseException e) {
				System.err.println("Error parsing and formatting string");
			}
			list.add(rs.getString(2) + ";" + printDate + ";" + rs.getString(4) + ";" + rs.getString(8));
		}

		rs.close();
		statement.close();

		return list;
	}

	// Cerca nel database un'allerta avente i valori corrispondenti a quelli passati
	// come parametri.
	// In caso positivo, segna l'allerta come avvenuta (occur=true)
	public void confirmAlert(int cap, String confirmDate, short timeSlot, short level, char type) throws SQLException {
		String date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse(confirmDate));
		} catch (ParseException e) {
			System.err.println("Error parsing string");
		}
		Statement statement = connection.createStatement();
		String queryUpdate = "UPDATE alert SET occur='true'" + " WHERE cap=" + cap + " AND date='" + date
				+ "' AND timeslot=" + timeSlot + " AND level=" + level + " AND type='" + type + "'";
		statement.executeUpdate(queryUpdate);
		statement.close();
	}

	// Elimina dal database l'allerta avente i valori corrispondenti a quelli
	// passati come parametri
	public void deleteAlert(int cap, String deleteDate, short timeSlot, char type) throws SQLException {
		String date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse(deleteDate));
		} catch (ParseException e) {
			System.err.println("Error parsing string");
		}
		Statement statement = connection.createStatement();
		String queryDelete = "DELETE FROM alert WHERE cap=" + cap + " AND date='" + date + "' AND timeSlot=" + timeSlot
				+ " AND type='" + type + "'";
		statement.executeUpdate(queryDelete);
		statement.close();
	}

	// Aggiorna il database inserendo nuove allerte oppure aggiornando i valori di
	// quelle gi presenti
	public void updateDatabase(ArrayList<Allerta> list) throws SQLException {
		String description = "";
		for (Allerta temp : list) {
			description = "";
			if (temp.getLevel() == 1) {
				if (temp.getType() == 'E') {
					description = "Moderate earthquake";
				} else if (temp.getType() == 'S') {
					description = "Moderate snow";
				} else if (temp.getType() == 'W') {
					description = "Moderate wind";
				} else {
					description = "Moderate rain";
				}
			} else if (temp.getLevel() == 2) {
				if (temp.getType() == 'E') {
					description = "Strong earthquake";
				} else if (temp.getType() == 'S') {
					description = "Strong snow";
				} else if (temp.getType() == 'W') {
					description = "Strong wind";
				} else {
					description = "Strong rain";
				}
			} else if (temp.getLevel() == 3) {
				if (temp.getType() == 'E') {
					description = "Heavy earthquake";
				} else if (temp.getType() == 'S') {
					description = "Heavy snow";
				} else if (temp.getType() == 'W') {
					description = "Heavy wind";
				} else {
					description = "Heavy rain";
				}
			}
			
			String date = null;
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("dd-MM-yyyy").parse(temp.getDate()));
			} catch (ParseException e) {
				System.err.println("Error parsing string");
			}
			
			String queryUpdate = "UPDATE alert SET level=" + (int) temp.getLevel() + ", description='" + description
					+ "' WHERE (cap=" + temp.getCap() + " AND date='" + date + "' AND timeslot="
					+ (int) temp.getTimeSlot() + " AND type='" + temp.getType() + "');";
			PreparedStatement statementUpdate = connection.prepareStatement(queryUpdate,Statement.RETURN_GENERATED_KEYS);
			int updateSuccessfull = statementUpdate.executeUpdate();
			if (updateSuccessfull == 0) {
				Statement statementInsert = connection.createStatement();
				String queryInsert = "INSERT INTO alert VALUES ('" + temp.getId() + "','" + temp.getCap() + "','" + date
						+ "','" + (int) temp.getTimeSlot() + "','" + (int) temp.getLevel() + "','" + temp.getType()
						+ "','" + Boolean.toString(temp.getValid()) + "','" + description + "');";
				statementInsert.executeUpdate(queryInsert);
				statementInsert.close();
			}
			statementUpdate.close();
		}
	}

	// Metodo eseguito quando l'utente apre la connessione col SistemaCentrale.
	// Estrae le allerte delle 24 ore successive per il suo cap
	public ArrayList<String> getDefaultAlert(int cap) throws SQLException {
		ArrayList<String> list = new ArrayList<String>();

		Date date = new Date();
		long timeT = date.getTime();
		Timestamp time = new Timestamp(timeT);
		String today = new SimpleDateFormat("yyyy-MM-dd").format(time);
		String currentHour = new SimpleDateFormat("HH").format(time);
		int currentTimeSlot = 1 + Integer.valueOf(currentHour) / 4;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1);
		String tomorrow = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

		Statement statement = connection.createStatement();

		String queryToday = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot>" + currentTimeSlot + " AND date='"
				+ today + "' ORDER BY date, timeslot";
		
		String queryTomorrow = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot<=" + currentTimeSlot
				+ " AND date='" + tomorrow + "' ORDER BY date, timeslot";
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		ResultSet todayResult = statement.executeQuery(queryToday);
		while (todayResult.next()) {
			String printDate = null;
			try {
				Date tempDate = sdf.parse(todayResult.getString("date"));
				printDate = new SimpleDateFormat("dd-MM-yyyy").format(tempDate);
			} catch (ParseException e) {
				System.err.println("Error parsing and formatting string");
			}
			
			list.add(todayResult.getString("cap") + ";" + printDate + ";"
					+ todayResult.getString("timeslot") + ";" + todayResult.getString("description"));
		}

		ResultSet tomorrowResult = statement.executeQuery(queryTomorrow);
		while (tomorrowResult.next()) {
			String printDate = null;
			try {
				Date tempDate = sdf.parse(tomorrowResult.getString("date"));
				printDate = new SimpleDateFormat("dd-MM-yyyy").format(tempDate);
			} catch (ParseException e) {
				System.err.println("Error parsing and formatting string");
			}
			list.add(tomorrowResult.getString("cap") + ";" + printDate + ";"
					+ tomorrowResult.getString("timeslot") + ";" + tomorrowResult.getString("description"));
		}

		todayResult.close();
		tomorrowResult.close();
		statement.close();

		return list;
	}

	// Estrae le allerte valide, per il cap dell'utente, per la fascia oraria
	// corrente
	public ArrayList<String> createNotice(int cap) throws SQLException {
		ArrayList<String> list = new ArrayList<String>();
		Date date = new Date();
		long timeT = date.getTime();
		Timestamp time = new Timestamp(timeT);
		String today = new SimpleDateFormat("yyyy-MM-dd").format(time);
		String currentHour = new SimpleDateFormat("HH").format(time);
		int currentTimeSlot = 1 + Integer.valueOf(currentHour) / 4;

		Statement statement = connection.createStatement();
		String query = "SELECT * FROM alert WHERE cap=" + cap + " AND timeslot=" + currentTimeSlot + " AND date='"
				+ today + "' ORDER BY level";
		ResultSet rs = statement.executeQuery(query);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		while (rs.next()) {
			String printDate = null;
			try {
				Date tempDate = sdf.parse(rs.getString("date"));
				printDate = new SimpleDateFormat("dd-MM-yyyy").format(tempDate);
			} catch (ParseException e) {
				System.out.println("Error parsing and formatting string");
			}
			list.add(rs.getString("cap") + ";" + printDate + ";" + rs.getString("timeslot") + ";"
					+ rs.getString("description"));
		}

		rs.close();
		statement.close();

		return list;
	}

	// Restituisce il valore da assegnare come id alla prima allerta generata
	// dall'esecuzione del software
	public int getIndex() throws SQLException {
		int index = 0;
		Statement statement = connection.createStatement();
		String queryCounter = "SELECT MAX(id) FROM alert";
		ResultSet rs = statement.executeQuery(queryCounter);
		if (rs.next()) {
			@SuppressWarnings("unused")
			String value = rs.getString(1);
			if (rs.wasNull()) {
				return index + 1;
			} else {
				index = Integer.valueOf(rs.getString(1));
			}
		}
		rs.close();
		statement.close();
		return index + 1;
	}

	// Chiude la connessione con il database
	public void closeConnection() throws SQLException {
		connection.close();
	}

	// Chiude la connessione con il database
	public void openConnection() {
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			connection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
			// System.out.println("Connection with database established");
			initialiseDB();
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("ConnettoreDB Error: cannot connection to local database (" + e.toString() + ")");
			// e.printStackTrace();
		}
	}

	// Ritorna true se la connessione � chiusa
	public boolean isClose() throws SQLException {
		return connection.isClosed();
	}
}
