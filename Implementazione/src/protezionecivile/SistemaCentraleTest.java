package protezionecivile;

import serverRMI.ServerAppPrevisioni;
import serverRMI.ServerSisCentrale;
import tools.Utility;
import appprevisione.AppPrevisioni;
import appprevisione.Sensore;

import static org.junit.Assert.*;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SistemaCentraleTest implements Remote {

	private static int INDEX = 0;
	private String[] testAppPrevisioni = { "AppPrevisione.RegioneUnoTest", "AppPrevisione.RegioneDueTest" };

	@Before
	// Inizializza la connessione col database e avvia rmiregistry automaticamente
	// oppure attende che sia l'utente a farlo
	public void setUp() {
		Utility.startRMIRegistry();
	}

	@After
	// Chiude la connessione al database, ferma l'esecuzione di tutte le
	// AppPrevisioni e del SistemaCentrale
	// Esegue oppure verifica la chiusura di rmiregistry
	public void tearDown() throws SQLException {
		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			ServerSisCentrale stub = (ServerSisCentrale) registry.lookup("SistemaCentrale");
			stub.stopSystem();
			for (String s : testAppPrevisioni) {
				ServerAppPrevisioni stubApp = (ServerAppPrevisioni) registry.lookup(s);
				stubApp.stopAppPrevisioni();
			}
			registry.unbind("Sensor.99999.S");
			registry.unbind("Sensor.99999.W");
			registry.unbind("Sensor.99999.E");
			registry.unbind("Sensor.99999.R");
			registry.unbind("Sensor.99998.S");
			registry.unbind("Sensor.99998.W");
			registry.unbind("Sensor.99998.E");
			registry.unbind("Sensor.99998.R");
		} catch (Exception e) {
			// e.printStackTrace();
		}

		Utility.stopRMIRegistry();
		
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			connection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
			System.out.println("Connection with database established");
		} catch (ClassNotFoundException | SQLException e1) {
			System.err.println("ConnettoreDB Error: cannot connect to local database (" + e1.toString() + ")");
			// e.printStackTrace();
		}
		String queryDelete = "DELETE FROM alert WHERE cap=99999 OR cap=99998";
		Statement statement = connection.createStatement();
		statement.executeUpdate(queryDelete);
		statement.close();
		connection.close();
		connection = null;
	}

	@Test
	@SuppressWarnings("unused")
	// Crea un'istanza SistemaCentrale e verifica il corretto avviamento del
	// relativo server RMI
	public void testServerSisCentrale() {
		ArrayList<Integer> testCapRegionOne = new ArrayList<Integer>();
		testCapRegionOne.add(99999);
		ArrayList<Integer> testCapRegionTwo = new ArrayList<Integer>();
		testCapRegionTwo.add(99998);

		SistemaCentrale testSystem = SistemaCentrale.getInstance();

		ServerSisCentrale stub = null;
		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			stub = (ServerSisCentrale) registry.lookup("SistemaCentrale");
			assertNotNull(stub);
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	@Test
	// Crea un oggetto AppPrevisioni e verifica il corretto collegamento al suo
	// server RMI
	public void testConnectionServerAppPrevisioni() {
		ArrayList<Integer> verifyCap = new ArrayList<Integer>();
		ArrayList<Integer> testCap = new ArrayList<Integer>();

		SistemaCentrale testSystem = SistemaCentrale.getInstance();

		ArrayList<String> appList = new ArrayList<String>();
		appList.add("AppPrevisione.RegioneUnoTest");

		Sensore s1 = new Sensore(99999, 'S');
		Sensore s2 = new Sensore(99999, 'W');
		Sensore s3 = new Sensore(99999, 'R');
		Sensore s4 = new Sensore(99999, 'E');

		testCap.add(99999);

		AppPrevisioni testAppOne = new AppPrevisioni("AppPrevisione.RegioneUnoTest", testCap);
		testSystem.setAppList(appList);

		ServerAppPrevisioni stub = null;
		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			stub = (ServerAppPrevisioni) registry.lookup("AppPrevisione.RegioneUnoTest");
			verifyCap.addAll(stub.getCapList());
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		assertEquals("Numero di cap corretto", Integer.toString(testCap.size()), Integer.toString(verifyCap.size()));
		assertEquals("Cap corretto", Integer.toString(testCap.get(0)), Integer.toString(verifyCap.get(0)));
	}

	@SuppressWarnings("unused")
	@Test
	// Verifica il metodo che stabilisce se un cap � presente nel sistema
	public void testCheckCapList() {
		ArrayList<Integer> testCapRegionOne = new ArrayList<Integer>();
		testCapRegionOne.add(99999);
		ArrayList<Integer> testCapRegionTwo = new ArrayList<Integer>();
		testCapRegionTwo.add(99998);

		SistemaCentrale testSystem = SistemaCentrale.getInstance();

		Sensore s1 = new Sensore(99999, 'S');
		Sensore s2 = new Sensore(99999, 'W');
		Sensore s3 = new Sensore(99999, 'R');
		Sensore s4 = new Sensore(99999, 'E');
		Sensore s5 = new Sensore(99998, 'S');
		Sensore s6 = new Sensore(99998, 'W');
		Sensore s7 = new Sensore(99998, 'R');
		Sensore s8 = new Sensore(99998, 'E');

		AppPrevisioni testAppOne = new AppPrevisioni("AppPrevisione.RegioneUnoTest", testCapRegionOne);
		AppPrevisioni testAppTwo = new AppPrevisioni("AppPrevisione.RegioneDueTest", testCapRegionTwo);
		ArrayList<String> testAppList = new ArrayList<String>();
		testAppList.add("AppPrevisione.RegioneUnoTest");
		testAppList.add("AppPrevisione.RegioneDueTest");

		testSystem.setAppList(testAppList);

		boolean testOne = false, testTwo = true, testThree = false;
		ServerSisCentrale stub = null;
		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			stub = (ServerSisCentrale) registry.lookup("SistemaCentrale");
			Integer testCapOne = 99999;
			Integer testCapTwo = 99998;
			Integer testCapThree = 99997;
			testOne = stub.checkInCapList(testCapOne);
			testTwo = stub.checkInCapList(testCapTwo);
			testThree = !stub.checkInCapList(testCapThree);
			assertTrue(testOne && testTwo && testThree);
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	@Test
	// Verifica il metodo che restituisce l'elenco dei cap presenti nel sistema,
	// divisi per AppPrevisioni
	public void testGetCapList() {
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<Integer> testCapRegionOne = new ArrayList<Integer>();
		testCapRegionOne.add(99999);
		ArrayList<Integer> testCapRegionTwo = new ArrayList<Integer>();
		testCapRegionTwo.add(99998);

		SistemaCentrale testSystem = SistemaCentrale.getInstance();

		Sensore s1 = new Sensore(99999, 'S');
		Sensore s2 = new Sensore(99999, 'W');
		Sensore s3 = new Sensore(99999, 'R');
		Sensore s4 = new Sensore(99999, 'E');
		Sensore s5 = new Sensore(99998, 'S');
		Sensore s6 = new Sensore(99998, 'W');
		Sensore s7 = new Sensore(99998, 'R');
		Sensore s8 = new Sensore(99998, 'E');

		AppPrevisioni testAppOne = new AppPrevisioni("AppPrevisione.RegioneUnoTest", testCapRegionOne);
		AppPrevisioni testAppTwo = new AppPrevisioni("AppPrevisione.RegioneDueTest", testCapRegionTwo);
		ArrayList<String> testAppList = new ArrayList<String>();
		testAppList.add("AppPrevisione.RegioneUnoTest");
		testAppList.add("AppPrevisione.RegioneDueTest");

		testSystem.setAppList(testAppList);

		ServerSisCentrale stub = null;
		try {
			Registry registry = LocateRegistry.getRegistry("localhost");
			stub = (ServerSisCentrale) registry.lookup("SistemaCentrale");
			list = stub.getCapList();
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		assertEquals("Numero di cap corretto", Integer.toString(list.size()), Integer.toString(2));
	}

	@Test
	// Verifica la connessione al database
	public void testConnessioneDatabase() throws SQLException {
		Connection testConnection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			testConnection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
			System.out.println("Connection with database established");
			initialiseDB(testConnection);
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("ConnettoreDB Error: cannot connect to local database (" + e.toString() + ")");
		}

		try {
			testConnection.close();
		} catch (NullPointerException e) {
			// e.printStackTrace();
		}
		assertTrue(testConnection.isClosed());
		testConnection = null;
	}

	@SuppressWarnings("unused")
	@Test
	// Verifica la correttezza del contatore da assegnare alle nuove allerte
	public void testContatore() throws SQLException {
		Connection testConnection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			testConnection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
			System.out.println("Connection with database established");
			initialiseDB(testConnection);
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("ConnettoreDB Error: cannot connect to database (" + e.toString() + ")");
		}

		int temp = 0;
		Statement statement = testConnection.createStatement();
		String query = "SELECT MAX(id) FROM alert";
		ResultSet rs = statement.executeQuery(query);
		if (rs.next()) {
			String value = rs.getString(1);
			if (rs.wasNull()) {
				temp = 0;
			} else {
				temp = Integer.valueOf(rs.getString(1));
			}
		}
		rs.close();
		statement.close();
		setCounter(testConnection);
		testConnection.close();
		assertEquals("Contatore corretto", INDEX - 1, temp);
	}

	@Test
	// Verifica la correttezza del metodo per incrementare il contatore
	public void testIncrement() throws RemoteException {
		int temp = INDEX;
		getCounterAndIncrement();
		assertEquals("Incremento corrretto", temp + 1, getCounter());
	}

	// Medoto che assegna alla variabile index un valore corrispondente all'id
	// maggiore presente nel database + 1
	public void setCounter(Connection connection) throws SQLException {
		connection.close();
		connection = null;
		AccessoDB connector = new ConnettoreDB();
		INDEX = connector.getIndex();
		connector.closeConnection();
		connector = null;
	}

	// Incrementa index e ne ritorna il valore
	private int getCounterAndIncrement() {
		return INDEX++;
	}

	private int getCounter() {
		return INDEX;
	}

	// Verifica che nel database sia presente la tabella "alert" e, nel caso non ci
	// fosse, la crea
	private void initialiseDB(Connection connection) throws SQLException {
		// check if there is the table
		DatabaseMetaData dbm = connection.getMetaData();
		ResultSet rs = dbm.getTables(null, null, "alert", null);

		// if not exist table alert, create alert table
		if (rs.next()) {
		} else {
			Statement st = connection.createStatement();
			st.executeUpdate(
					"CREATE TABLE alert(" + "id INT PRIMARY KEY," + "cap INT," + "date DATE," + "timeslot INT,"
							+ "level INT," + "type CHAR(1)," + "occur VARCHAR(45)," + "description VARCHAR(45)" + ");");
			System.out.println("created the table 'alert'");
		}
		
		rs.close();
	}

}
