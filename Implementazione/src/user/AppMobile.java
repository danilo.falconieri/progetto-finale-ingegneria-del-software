package user;

import serverRMI.ServerSisCentrale;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class AppMobile implements Runnable {
    protected int cap;
    protected ServerSisCentrale stubSisCentrale = null;
    protected ScheduledExecutorService executorService;
    protected ArrayList<String> noticesList;

    public AppMobile(int cap) {
        this.cap = cap;
        noticesList = new ArrayList<>();

        // lookup del sistema centrale per mantenere connessione persistente
        try {
            Registry reg = LocateRegistry.getRegistry("localhost");
            stubSisCentrale = (ServerSisCentrale) reg.lookup("SistemaCentrale");
        } catch (RemoteException | NotBoundException e) {
            System.err.println("AppMobile Error: cannot connection to StubCentralSystem (" + e.toString() + ")");
            // e.printStackTrace();
        }

        //creo uno ScheduledExecutorService che lavora come un Timer. Inizia con un Delay pari a millisToNextFourHour() e ripete this.run() ogni 4 ore
        executorService = Executors.newSingleThreadScheduledExecutor();

        //REAL
        executorService.scheduleAtFixedRate(this, millisToNextFourHourPlusOne(), 14400000, TimeUnit.MILLISECONDS);
    }

    // ogni quattro ore più un minuto, chiede al sistema centrale le nuove notifiche di allarmi
    @Override
    public void run() {
        System.out.println("APPMOBILE IS RUNNING()");

        noticesList.clear();
        noticesList = new ArrayList<>(getNotice());
    }

    // spegne l'executorService, cioè blocca l'esecuzuione ripetuta del metodo run
    public void stopAppMobile() {
        executorService.shutdown();
        stubSisCentrale = null;
    }

    // ritorna il valore di ritono della metodo checkInCapList del sistema centrale, cioè ritorna true se il parametro cap è presente nella lista dei cap validi
    protected boolean checkInCapList(Integer cap) {
        try {
            return stubSisCentrale.checkInCapList(cap);
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for checkInCapList method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return false;
    }

    // ritorna , il valore di ritorno del metodo getCapList del sistema centrale
    protected ArrayList<String> getCapList() {
        ArrayList<String>  out = null;
        try {
            out = new ArrayList<>(stubSisCentrale.getCapList());
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for getCapList method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return out;
    }

    // ritorna il valore di ritorno del metodo askNotice per il proprio cap
    public ArrayList<String> getNotice() {
        ArrayList<String> out = null;
        try {
            out = new ArrayList<>(stubSisCentrale.askNotice(this.cap));
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for askNotice method (" + e.toString() + ")");
            //e.printStackTrace();
        }
        return out;
    }

    // ritorna il valore di ritorno del metodo getDefaultAlert, cioè la lista degli allarmi di default per il proprio cap
    protected ArrayList<String> getDefaultAlert() {
        ArrayList<String> out = null;
        try {
            out = new ArrayList<>(stubSisCentrale.getDefaultAlert(this.cap));
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for getDefaultAlert method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return out;
    }

    // ritorna il valore di ritorno del metodo listAlarm con parametri nulli, cioè la lista di tutti gli allarmi di ogni cap
    protected ArrayList<String> getAllAlert() {
        ArrayList<String> out = null;
        try {
            out = new ArrayList<>(stubSisCentrale.listAlarm(null, null, null, null));
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for lisAlarm method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return out;
    }

    // ritorna il valore di ritorno del metodo listSeriousAlarm, cioè la lista degli allarmi in evidenza
    protected ArrayList<String> getSeriousAlarm() {
        ArrayList<String> out = null;
        try {
            out = new ArrayList<>(stubSisCentrale.listSeriousAlarm());
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for listSeriousAlarm method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return out;
    }

    // ritorna il valore di ritorno del metodo listAlarm in base ai parametri passati
    protected ArrayList<String> getListAlarm(Integer cap, Short timeSlot, Character type, String date){
        ArrayList<String> data = null;
        try{
            data = new ArrayList<>(stubSisCentrale.listAlarm(cap, timeSlot, type, date));
        }catch(RemoteException e){
            System.err.println("AppMobile Error: StubCentralSystem for listSeriousAlarm method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return data;
    }

    // ritorna la differenza in millisecondi tra l'ora attuale (now) e l'ora multipla di 4 sucessiva (next) più uno
    private long millisToNextFourHourPlusOne() {
    	
        LocalTime now = LocalTime.now().truncatedTo(ChronoUnit.MILLIS);
        LocalTime next;

        //seleziono qual è la fascia oraria successiva e ritorno la differenza
        int hour = now.getHour();
        //fascia 00:00:00 - 03:59:59
        if (hour < 4)
            next = LocalTime.of(4, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            //fascia 04:00:00 - 07:59:59
        else if (hour < 8)
            next = LocalTime.of(8, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            //fascia 08:00:00 - 11:59:59
        else if (hour < 12)
            next = LocalTime.of(12, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            //fascia 12:00:00 - 15:59:59
        else if (hour < 16)
            next = LocalTime.of(16, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            //fascia 16:00:00 - 19:59:59
        else if (hour < 20)
            next = LocalTime.of(20, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            //fascia 20:00:00 - 23:59:59
        else {
            //siccome in questo caso per fare la differenza con mezzanotte ho bisogno di sapere anche il giorno, uso i LocalDateTime (perchè devo fare la differenza con la mezzanotte del giorno seguente)
            LocalDateTime nowDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);
            LocalDateTime nextDateTime = null;
            try {
            	nextDateTime = LocalDateTime.of(nowDateTime.getYear(), nowDateTime.getMonth(), nowDateTime.getDayOfMonth() + 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);
            } catch (Exception e) {
            	try {
            		nextDateTime = LocalDateTime.of(nowDateTime.getYear(), nowDateTime.getMonth().plus(1), 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);	
            	} catch (Exception e1) {
            		nextDateTime = LocalDateTime.of(nowDateTime.getYear() + 1, Month.JANUARY, 1, 0, 0).truncatedTo(ChronoUnit.MILLIS);	
            	}
            }            Duration timeElapsed = Duration.between(nowDateTime, nextDateTime);
            return timeElapsed.toMillis();
        }
        Duration timeElapsed = Duration.between(now, next);
        return timeElapsed.toMillis();
    }


    // GETTERS AND SETTER SECTION
    protected int getCap() {
        return cap;
    }

    protected void setCap(int cap) {
        this.cap = cap;
    }

    protected ArrayList<String> getNoticesList() {
        return noticesList;
    }

    protected void setNoticesList(ArrayList<String> noticesList) {
        this.noticesList = noticesList;
    }

    public ScheduledExecutorService getExecutorService(){
        return executorService;
    }

    public void  setExecutorService(ScheduledExecutorService executorService){
        this.executorService = executorService;
    }
}
