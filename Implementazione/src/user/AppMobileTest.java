package user;

import appprevisione.AppPrevisioni;
import appprevisione.Sensore;
import protezionecivile.SistemaCentrale;
import serverRMI.ServerAppPrevisioni;
import serverRMI.ServerSisCentrale;
import tools.Utility;

import static org.junit.Assert.*;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ScheduledExecutorService;

import org.junit.After;
import org.junit.Test;

public class AppMobileTest {

	@After
	public void tearDown() throws SQLException {
	}
	   
	@Test
	@SuppressWarnings("unused")
    public void stopAppMobile() {
        Utility.startRMIRegistry();
        SistemaCentrale sistemaCentrale = SistemaCentrale.getInstance();
        Sensore s1 = new Sensore(99999, 'W');
        Sensore s2 = new Sensore(99999, 'R');
        Sensore s3 = new Sensore(99999, 'S');
        Sensore s4 = new Sensore(99999, 'E');
        AppPrevisioni testAppPrevisioni = new AppPrevisioni("AppPrevisione.testAppPrevisioni", new ArrayList<>(Arrays.asList(99999)));
        sistemaCentrale.setAppList(new ArrayList<String>(Arrays.asList("AppPrevisione.testAppPrevisioni")));
        AppMobile appMtest = new AppMobile(99999);


        ScheduledExecutorService exOLD = appMtest.getExecutorService();
        appMtest.stopAppMobile();
        assertTrue("AppMobile stoppata correttamente", appMtest.getExecutorService().isShutdown());


        try {
            Registry registry = LocateRegistry.getRegistry("localhost");
            ServerAppPrevisioni stubApp = (ServerAppPrevisioni) registry.lookup("AppPrevisione.testAppPrevisioni");
            stubApp.stopAppPrevisioni();
            ServerSisCentrale stub = (ServerSisCentrale) registry.lookup("SistemaCentrale");
            stub.stopSystem();
            registry.unbind("Sensor.99999.W");
            registry.unbind("Sensor.99999.R");
            registry.unbind("Sensor.99999.S");
            registry.unbind("Sensor.99999.E");
        } catch (Exception e) {
            System.err.println("tearDown() Error: ");
            e.printStackTrace();
        }
        appMtest.stopAppMobile();
        appMtest = null;
        Utility.stopRMIRegistry();
        
        Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			// database path, if it's new database, it will be created in the project folder
			connection = DriverManager.getConnection("jdbc:sqlite:DBSWENG.db");
		} catch (ClassNotFoundException | SQLException e1) {
			System.err.println("ConnettoreDB Error: cannot connect to local database (" + e1.toString() + ")");
			// e.printStackTrace();
		}
		String queryDelete = "DELETE FROM alert WHERE cap=99999";
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(queryDelete);
		} catch (SQLException ignore) {			
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (SQLException ignore) {
			}
		}
		connection = null;
    }

    @Test
    public void checkInCapList() {
        String mss = "La funzione che dovrebbe essere testata � user.AppMobile.checkInCapList(). Tale funzione richiama tramite rmi il metodo protezionecivile.SistemaCentrale.checkInCapList()\n" +
                     "la quale � gi� stata testata in protezionecivile.SistemaCentraleTest.testCheckCapList().";
        System.out.println();
        System.out.println(mss);
        assertTrue(true);
    }

    @Test
    public void getCapList() {
        String mss = "La funzione che dovrebbe essere testata � user.AppMobile.getCapList(). Tale funzione richiama tramite rmi il metodo protezionecivile.SistemaCentrale.getCapList()\n" +
                     "la quale � gi� stata testata in protezionecivile.SistemaCentraleTest.testGetCapList().";
        System.out.println();
        System.out.println(mss);
        assertTrue(true);
    }

    @Test
    public void getNotice() {
        String mss = "La funzione che dovrebbe essere testata � user.AppMobile.getNotice(). Tale funzione richiama tramite rmi il metodo protezionecivile.SistemaCentrale.askNotice()\n" +
                     "la quale a sua volta richiama protezionecivile.ConnettoreDB.createNotice() che � gi� stata testata in protezionecivile.ConnettoreDBTest.testQueryCreateNotice().";
        System.out.println();
        System.out.println(mss);
        assertTrue(true);
    }

    @Test
    public void getDefaultAlert() {
        String mss = "La funzione che dovrebbe essere testata � user.AppMobile.getDefaultAlert(). Tale funzione richiama tramite rmi il metodo protezionecivile.SistemaCentrale.getDefaultAlert()\n" +
                "la quale a sua volta richiama protezionecivile.ConnettoreDB.openConnection() che � gi� stata testata in protezionecivile.ConnettoreDBTest.testOpenConnection().";
        System.out.println();
        System.out.println(mss);
        assertTrue(true);
    }

    @Test
    public void getAllAlert() {
        String mss = "La funzione che dovrebbe essere testata � user.AppMobile.getDefaultAlert(). Tale funzione richiama tramite rmi il metodo protezionecivile.SistemaCentrale.listAlarm()\n" +
                "la quale a sua volta richiama protezionecivile.ConnettoreDB.getData() che � gi� stata testata in protezionecivile.ConnettoreDBTest.testQueryGetAlarm().";
        System.out.println();
        System.out.println(mss);
        assertTrue(true);
    }

    @Test
    public void getSeriousAlarm() {
        String mss = "La funzione che dovrebbe essere testata � user.AppMobile.getSeriousAlarm(). Tale funzione richiama tramite rmi il metodo protezionecivile.SistemaCentrale.listSeriousAlarm()\n" +
                "la quale a sua volta richiama protezionecivile.ConnettoreDB.getData() che � gi� stata testata in protezionecivile.ConnettoreDBTest.testQueryGetSeriousAlarm().";
        System.out.println();
        System.out.println(mss);
        assertTrue(true);
    }

}