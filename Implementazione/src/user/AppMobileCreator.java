package user;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import serverRMI.ServerSisCentrale;
import tools.Utility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

public class AppMobileCreator {

    private static AppMobileCreator instance = null;
    private JFrame frame = new JFrame("AppCreator");
    private JPanel mainPanel;
    private JButton OKButton;
    private JTextField inputCap;
    private JTextField inputName;
    private JMenu menu;
    private JMenuItem closeMenuItem;
    private JMenuItem infoCapList;
    private ServerSisCentrale stubSisCentrale = null;

    private AppMobileCreator() {
        // lookup del sistema centrale per mantenere connessione persistente
        try {
            Registry reg = LocateRegistry.getRegistry("localhost");
            stubSisCentrale = (ServerSisCentrale) reg.lookup("SistemaCentrale");
        } catch (RemoteException | NotBoundException err) {
            System.err.println("AppMobileWindowCreator cannot connect to CentralSystem (" + err.toString() + ")");
            //rr.printStackTrace();
        }

        //Inizializzo il frame
        frame.setContentPane(this.mainPanel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setPreferredSize(new Dimension(300, 200));
        frame.setResizable(false);
        // premendo enter, viene premuto OKButton
        frame.getRootPane().setDefaultButton(OKButton);
        frame.setVisible(true);
        frame.pack();

        // LISTENER SECTION
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                frame.setExtendedState(JFrame.ICONIFIED);
            }
        });

        // premendo il tasto OK, creo un appMobile, con i relativi controlli
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    // prendo i dati dai campi di input
                    Integer cap = Integer.parseInt(inputCap.getText().trim());
                    String name = inputName.getText().trim();

                    // controllo che sia stato inserito il nome
                    if (name.equals("")) {
                        displayErrorDialog("Insert name");
                        return;
                    }

                    // controllo che il cap inserito sia nella lista dei cap validi
                    try {
                        if (!stubSisCentrale.checkInCapList(cap)) {
                            String errMss = "This " + cap + " cap is not available.\nPlease insert one of this:\n\n";
                            for (String s : getCapList())
                                errMss += s + "\n";
                            displayErrorDialog(errMss);
                            return;
                        }
                    } catch (RemoteException ex) {
                        System.err.println("Errore Connessione AppPrevisione");
                        ex.printStackTrace();
                    }

                    // se i controlli sono passati, creo l'appMobile
                    AppMobileWindow newAppMobile = new AppMobileWindow(cap, name);

                    //resetto i campi di input
                    inputCap.setText(null);
                    inputName.setText(null);

                } catch (NumberFormatException err) {
                    // Caso in cui il parseInt non vadi a buon fine,
                    if (inputName.getText().equals(""))
                        displayErrorDialog("Insert cap and name");
                    else
                        displayErrorDialog("You forgot to insert cap");
                }
            }

        });

        closeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //yes case
                if (JOptionPane.showConfirmDialog(frame, "Closing this you will turn off the entire program. Are you sure?") == 0) {
                    frame.dispose();
                    try {
                        stubSisCentrale.stopSystem();
                    } catch (RemoteException err) {
                        //err.printStackTrace();
                    }
                    Utility.stopRMIRegistry();
                    instance = null;
                    Runtime.getRuntime().exit(0);
                }
                //no,cancel,exit case
            }
        });

        infoCapList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String errMss = "Insert a cap that is in this list:\n\n";
                for (String s : getCapList())
                    errMss += s + "\n";
                displayErrorDialog(errMss);
            }
        });

        MouseAdapter listener = new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (e.getSource().getClass().getSimpleName().equals("JMenuItem")) {
                    JMenuItem item = (JMenuItem) e.getSource();
                    item.setSelected(true);
                    item.setOpaque(true);
                    item.setBackground(new Color(163, 184, 204));
                } else if (e.getSource().getClass().getSimpleName().equals("JMenu")) {
                    JMenu item = (JMenu) e.getSource();
                    item.setSelected(true);
                    item.setOpaque(true);
                    item.setBackground(new Color(163, 184, 204));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (e.getSource().getClass().getSimpleName().equals("JMenuItem")) {
                    JMenuItem item = (JMenuItem) e.getSource();
                    item.setSelected(false);
                    item.setOpaque(false);
                    item.setBackground(UIManager.getColor("control"));
                } else if (e.getSource().getClass().getSimpleName().equals("JMenu")) {
                    JMenu item = (JMenu) e.getSource();
                    item.setSelected(false);
                    item.setOpaque(false);
                    item.setBackground(UIManager.getColor("control"));
                }
            }
        };
        menu.addMouseListener(listener);
        infoCapList.addMouseListener(listener);
    }

    private void displayErrorDialog(String message) {
        JOptionPane.showMessageDialog(this.frame, message);
    }

    protected ArrayList<String> getCapList() {
        ArrayList<String> out = null;
        try {
            out = new ArrayList<>(stubSisCentrale.getCapList());
        } catch (RemoteException e) {
            System.err.println("AppMobile Error: StubCentralSystem for getCapList method (" + e.toString() + ")");
            // e.printStackTrace();
        }
        return out;
    }

    public static AppMobileCreator getInstance() {
        if (instance == null)
            instance = new AppMobileCreator();

        return instance;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(8, 2, new Insets(1, 1, 1, 1), -1, -1));
        final JLabel label1 = new JLabel();
        label1.setText("Cap");
        mainPanel.add(label1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        inputCap = new JTextField();
        mainPanel.add(inputCap, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Name");
        mainPanel.add(label2, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        inputName = new JTextField();
        mainPanel.add(inputName, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final Spacer spacer1 = new Spacer();
        mainPanel.add(spacer1, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, new Dimension(10, 10), new Dimension(10, 10), new Dimension(10, 10), 0, false));
        final Spacer spacer2 = new Spacer();
        mainPanel.add(spacer2, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, new Dimension(10, 10), new Dimension(10, 10), new Dimension(10, 10), 0, false));
        OKButton = new JButton();
        OKButton.setText("OK");
        mainPanel.add(OKButton, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JMenuBar menuBar1 = new JMenuBar();
        menuBar1.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        menuBar1.setAlignmentX(0.0f);
        menuBar1.setAlignmentY(0.0f);
        menuBar1.setBorderPainted(true);
        menuBar1.putClientProperty("html.disable", Boolean.TRUE);
        mainPanel.add(menuBar1, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        menu = new JMenu();
        menu.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        menu.setMargin(new Insets(0, 0, 0, 0));
        menu.setText("Menu");
        menuBar1.add(menu);
        closeMenuItem = new JMenuItem();
        closeMenuItem.setText("close");
        menu.add(closeMenuItem);
        infoCapList = new JMenuItem();
        infoCapList.setOpaque(false);
        infoCapList.setRequestFocusEnabled(true);
        infoCapList.setText("Info Cap List");
        menuBar1.add(infoCapList);
        final JLabel label3 = new JLabel();
        label3.setText("Insert fields to create a new AppMobile");
        mainPanel.add(label3, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        final JSeparator separator1 = new JSeparator();
        mainPanel.add(separator1, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
