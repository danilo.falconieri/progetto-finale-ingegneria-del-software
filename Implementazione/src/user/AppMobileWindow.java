package user;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AppMobileWindow extends AppMobile {
    private static final int NORMAL_WIDTH = 360;
    private static final int NORMAL_HEIGHT = 300;
    private static final int TABLEDISPLAY_WIDTH = 900;
    private static final int TABLEDISPLAY_HEIGHT = 700;
    private static final int ALERTDISPLAY_WIDTH = 550;
    private static final int ALERTDISPLAY_HEIGHT = 400;


    private JFrame frame = new JFrame("AppMobile");
    private JPanel rootPanel;
    private JPanel mainPanel;
    private JLabel title;
    private JPanel tableDisplayPanel;
    private JTable tableDisplay;
    private JPanel searchPanel;
    private JButton searchButton;
    private JTextField inputCap;
    private JComboBox inputTimeSlot;
    private JComboBox inputType;
    private JMenuItem changeCapMenuItem;
    private JMenuBar menuBar;
    private JMenuItem homeMenuItem;
    private JMenuItem customSearchMenuItem;
    private JMenuItem changeNameMenuItem;
    private JMenu changeMenu;
    private JMenu searchMenu;
    private JMenuItem seriousMenuItem;
    private JMenuItem allDataMenuItem;
    private JMenuItem infoCapList;
    private JLabel showCounterNotify;
    private JMenuItem notifyMenuItem;
    private JPanel alertDisplayPanel;
    private JTextPane alerDisplayTextPane;
    private JDateChooser inputDate;
    private JButton resetButton;
    private JMenuItem currentMenuItem;
    private ButtonGroup menuItem;
    private String name;
    private String activeMainPanelCard;
    private boolean activeShowDefaultAlert;


    public AppMobileWindow(int cap, String name) {
        super(cap);
        this.name = name.toUpperCase();

        $$$setupUI$$$();
        showDefaultTitle();
        frame.setContentPane(this.rootPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(NORMAL_WIDTH, NORMAL_HEIGHT));
        frame.setResizable(false);
        searchPanel.getRootPane().setDefaultButton(searchButton);
        frame.pack();

        // iniziallizo le seguenti var perchè il primo pannello che apro è il defaultAlert
        activeMainPanelCard = "tableDisplayPanelCard";
        activeShowDefaultAlert = true;

        run();
        frame.setVisible(true);

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //get input data
                Integer cap = null;
                Short timeSlot = null;
                Character type = null;
                String date = getInputDateFormatted();

                //get cap input
                try {
                    boolean isNull = false;
                    if (inputCap.getText().length() == 0) {
                        cap = null;
                        isNull = true;
                    }

                    if (!isNull) {
                        cap = Integer.parseInt(inputCap.getText().trim());

                        //check if cap is valid
                        if (!checkInCapList(cap)) {
                            String errMss = "Cap not valid, please enter one of this: \n\n";
                            for (String s : getCapList())
                                errMss += s + "\n";
                            displayErrorDialog(errMss);
                            return;
                        }
                    }
                } catch (NumberFormatException err) {
                    //System.err.println("CAP NOT VALID");
                    //  err.printStackTrace();
                    String errMss = "Cap not valid, please enter one of this: \n";
                    for (String s : getCapList())
                        errMss += s + "\n";
                    displayErrorDialog(errMss);
                    return;
                }

                //get input timeSlot
                timeSlot = getValueInputTimeSlot();

                //get input type
                type = getValueInputType();

                if (cap == null && timeSlot == null && type == null && date == null) {
                    displayErrorDialog("Insert at least one field");
                    return;
                }

                // execute query and show data
                ArrayList<String> data = new ArrayList<>(getListAlarm(cap, timeSlot, type, date));
                clearInputSearchPanel();
                showDataTable(data);
            }
        });
        // listener per visualizzare effetto selezionato a video
        MouseAdapter listener = new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (e.getSource().getClass().getSimpleName().equals("JMenuItem")) {
                    JMenuItem item = (JMenuItem) e.getSource();
                    item.setSelected(true);
                    item.setOpaque(true);
                    item.setBackground(new Color(163, 184, 204));
                } else if (e.getSource().getClass().getSimpleName().equals("JMenu")) {
                    JMenu item = (JMenu) e.getSource();
                    item.setSelected(true);
                    item.setOpaque(true);
                    item.setBackground(new Color(163, 184, 204));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (e.getSource().getClass().getSimpleName().equals("JMenuItem")) {
                    JMenuItem item = (JMenuItem) e.getSource();
                    item.setSelected(false);
                    item.setOpaque(false);
                    item.setBackground(UIManager.getColor("control"));
                } else if (e.getSource().getClass().getSimpleName().equals("JMenu")) {
                    JMenu item = (JMenu) e.getSource();
                    item.setSelected(false);
                    item.setOpaque(false);
                    item.setBackground(UIManager.getColor("control"));
                }
            }
        };
        homeMenuItem.addMouseListener(listener);
        notifyMenuItem.addMouseListener(listener);
        searchMenu.addMouseListener(listener);
        changeMenu.addMouseListener(listener);

        homeMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDefaultInTable();
            }
        });
        allDataMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAllAlarmTable();
            }
        });
        seriousMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showSeriousAlarmTable();
            }
        });
        currentMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //flag per terminare il ciclo se il cap inserito è valido oppure se ho premuto il tasto canc
                boolean endWhile = true;
                String cap = null;
                String defaultMess = "[Null is equal to all cap]\nPlease insert valid cap: ";
                String errMss = "";

                //ripeti il ciclo di visualizzazione showInputDialog finchè il cap inserito è valido o è stato premuto canc
                do {
                    try {
                        do {
                            cap = (String) displayInputDialog(errMss + defaultMess);
                            if (cap == null)
                                return;
                            if (cap.equals("")) {
                                cap = null;
                                break;
                            }

                            errMss = "Cap not valid, please enter one of this: \n\n";
                            for (String s : getCapList())
                                errMss += s + "\n";
                            defaultMess = "\n[Null is equal to all cap]";
                        } while (!checkInCapList(Integer.valueOf(cap)));

                        endWhile = false;
                    } catch (NumberFormatException err) {
                        boolean isNullException = err.getLocalizedMessage().split("\\n")[0].split(":")[0].contains("null");
                        if (isNullException) {
                            endWhile = false;
                        } else {
                            System.err.println("Format not valid " + err);
                            // err.printStackTrace();
                        }
                    }
                } while (endWhile);

                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String date = sdf.format(c.getTime());

                // execute query and show data

                ArrayList<String> data;
                if (cap == null)
                    data = new ArrayList<>(getListAlarm(null, null, null, date));
                else
                    data = new ArrayList<>(getListAlarm(Integer.valueOf(cap), null, null, date));
                clearInputSearchPanel();
                showDataTable(data);
            }
        });
        customSearchMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearInputSearchPanel();
                activeSearchPanel();
            }
        });
        infoCapList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String errMss = "Insert a cap that is in this list:\n\n";
                for (String s : getCapList())
                    errMss += s + "\n";
                displayErrorDialog(errMss);
            }
        });

        changeCapMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //flag per terminare il ciclo se il cap inserito è valido oppure se ho premuto il tasto canc
                boolean endWhile = true;
                String newCap = null;
                String defaultMess = "Please insert valid cap: ";
                String errMss = "";

                //ripeti il ciclo di visualizzazione showInputDialog finchè il cap inserito è valido o è stato premuto canc
                do {
                    try {
                        do {
                            newCap = (String) displayInputDialog(errMss + defaultMess);
                            if (newCap == null)
                                return;
                            errMss = "Cap not valid, please enter one of this: \n\n";
                            for (String s : getCapList())
                                errMss += s + "\n";
                            defaultMess = "";
                        } while (!checkInCapList(Integer.valueOf(newCap)));
                        AppMobileWindow.super.setCap(Integer.valueOf(newCap));
                        showDefaultTitle();

                        endWhile = false;
                    } catch (NumberFormatException err) {
                        boolean isNullException = err.getLocalizedMessage().split("\\n")[0].split(":")[0].contains("null");
                        if (isNullException) {
                            endWhile = false;
                        } else {
                            //System.err.println("Format not valid " + err);
                            // err.printStackTrace();
                        }
                    }
                } while (endWhile);
                setCap(Integer.valueOf(newCap));
                if (!activeMainPanelCard.equals("searchPanelCard"))
                    showDefaultInTable();
            }
        });
        changeNameMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newName = null;
                String errMss = "P";

                do {
                    newName = (String) displayInputDialog(errMss + "lease insert valid name: ");
                    if (newName == null)
                        return;
                    errMss = "Name not valid, p";
                } while (newName.equals(""));
                setName(newName.toUpperCase());
                showDefaultTitle();
            }
        });

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                //close thread of AppMobile when closing the AppMobileWindows
                stopAppMobile();
            }
        });
        notifyMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                activeAlertDisplayPanel();
                resetCounterNotify();
                showNoticesInAlertPanel();
            }
        });
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inputDate.setDate(null);
            }
        });
    }

    @Override
    public void run() {
        super.run();
        //testato se il frame è chiuso allora stoppo l'EX, non tanto utile, ma solo di conferma
        if (!frame.isDisplayable())
            stopAppMobile();

        if (SwingUtilities.isEventDispatchThread())
            refreshHomeAndNotify();
        else
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        refreshHomeAndNotify();
                    }
                });
            } catch (InterruptedException | InvocationTargetException e) {
                //e.printStackTrace();
                System.err.println("AppMobileWIndow Error: cannot execute run method with EDT (" + e.toString() + ")");
            }
    }

    private void refreshHomeAndNotify() {
        clearAlertPanel();

        //refresh home solo quando sono nel Home panel ogni 4 ore
        //(attivo il pannello di visualizzazione tabella e ho premuto il pulsante home)
        if (activeMainPanelCard.equals("tableDisplayPanelCard") && activeShowDefaultAlert)
            showDefaultInTable();

        if (activeMainPanelCard.equals("alertDisplayPanelCard")) {
            showNoticesInAlertPanel();
        } else {
            setCounterNotify(noticesList.size());
        }
    }

    private void clearAlertPanel() {
        alerDisplayTextPane.setText("");
    }

    private void showNoticesInAlertPanel() {
        String out = "";
        for (String s : noticesList) {
            String[] splitted = s.split(";");
            String time = "";

            switch (splitted[2]) {
                case "1":
                    time = "00:00 - 03:59";
                    break;
                case "2":
                    time = "04:00 - 07:59";
                    break;
                case "3":
                    time = "08:00 - 11:59";
                    break;
                case "4":
                    time = "12:00 - 15:59";
                    break;
                case "5":
                    time = "16:00 - 19:59";
                    break;
                case "6":
                    time = "20:00 - 23:59";
                    break;
            }

            out = out + "For time slot (" + time + " of " + splitted[1] + "), in your cap will be " + splitted[3].toUpperCase() + "\n";
        }
        alerDisplayTextPane.setText(out);
    }

    private void showDefaultTitle() {
        title.setText(this.name + ", your cap is: " + super.getCap());
    }

    private void setColumnTable(boolean id, boolean cap, boolean date, boolean timeslot, boolean level, boolean type, boolean occur, boolean description) {
        DefaultTableModel model = new DefaultTableModel();
        if (id)
            model.addColumn("Id");
        if (cap)
            model.addColumn("Cap");
        if (date)
            model.addColumn("Date");
        if (timeslot)
            model.addColumn("Timeslot");
        if (level)
            model.addColumn("Level");
        if (type)
            model.addColumn("Type");
        if (occur)
            model.addColumn("Occur");
        if (description)
            model.addColumn("Description");
        tableDisplay.setModel(model);
    }

    private void setRowTable(ArrayList<String> data) {
        if (data.size() != 0) {
            DefaultTableModel model = (DefaultTableModel) tableDisplay.getModel();

            //parsing of date and show row
            for (String s : data) {
                String[] out = s.split(";");

                model.addRow(out);
            }
        }
    }

    private void showDataTable(ArrayList<String> date) {
        setColumnTable(true, true, true, true, true, true, true, true);
        setRowTable(date);
        activeTableDisplayPanel();
        activeShowDefaultAlert = false;
    }

    private void showAllAlarmTable() {
        setColumnTable(true, true, true, true, true, true, true, true);
        setRowTable(getAllAlert());
        activeTableDisplayPanel();
        activeShowDefaultAlert = false;
    }

    private void showDefaultInTable() {
        setColumnTable(false, true, true, true, false, false, false, true);
        setRowTable(getDefaultAlert());
        activeTableDisplayPanel();
        activeShowDefaultAlert = true;
    }

    private void showSeriousAlarmTable() {
        setColumnTable(false, true, true, true, false, false, false, true);
        setRowTable(getSeriousAlarm());
        activeTableDisplayPanel();
        activeShowDefaultAlert = false;
    }

    private void clearInputSearchPanel() {
        inputCap.setText("");
        inputTimeSlot.setSelectedIndex(0);
        inputType.setSelectedIndex(0);
        inputDate.setDate(null);
    }

    private void displayErrorDialog(String message) {
        JOptionPane.showMessageDialog(this.frame, message);
    }

    private Object displayInputDialog(String message) {
        return JOptionPane.showInputDialog(this.frame, message);
    }

    private void activeTableDisplayPanel() {
        frame.setPreferredSize(new Dimension(TABLEDISPLAY_WIDTH, TABLEDISPLAY_HEIGHT));
        frame.pack();

        CardLayout mainPanelLayout = (CardLayout) mainPanel.getLayout();
        mainPanelLayout.show(mainPanel, "tableDisplayPanelCard");
        activeMainPanelCard = "tableDisplayPanelCard";
    }

    private void activeSearchPanel() {
        frame.setPreferredSize(new Dimension(NORMAL_WIDTH, NORMAL_HEIGHT));
        frame.pack();

        CardLayout mainPanelLayout = (CardLayout) mainPanel.getLayout();
        mainPanelLayout.show(mainPanel, "searchPanelCard");
        activeMainPanelCard = "searchPanelCard";
        activeShowDefaultAlert = false;
    }

    private void activeAlertDisplayPanel() {
        frame.setPreferredSize(new Dimension(ALERTDISPLAY_WIDTH, ALERTDISPLAY_HEIGHT));
        frame.pack();

        CardLayout mainPanelLayout = (CardLayout) mainPanel.getLayout();
        mainPanelLayout.show(mainPanel, "alertDisplayPanelCard");
        activeMainPanelCard = "alertDisplayPanelCard";
    }

    private void setCounterNotify(int newNumber) {
        showCounterNotify.setText(Integer.toString(newNumber));
    }

    private void resetCounterNotify() {
        showCounterNotify.setText("0");
    }

    //metodo che ritorna il valore del inputDate. Se è null ritorna null, se ha un errore di formato ritorna FORMAT_ERROR
    public String getInputDateFormatted() {
        if (inputDate.getDate() == null)
            return null;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf.format(inputDate.getDate());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Short getValueInputTimeSlot() {
        Short timeSlot = null;
        int index = inputTimeSlot.getSelectedIndex();
        switch (index) {
            case 0:
                timeSlot = null;
                break;
            case 1:
                timeSlot = 1;
                break;
            case 2:
                timeSlot = 2;
                break;
            case 3:
                timeSlot = 3;
                break;
            case 4:
                timeSlot = 4;
                break;
            case 5:
                timeSlot = 5;
                break;
            case 6:
                timeSlot = 6;
                break;

        }
        return timeSlot;
    }

    public Character getValueInputType() {
        Character type = null;
        int index = inputType.getSelectedIndex();
        switch (index) {
            case 0:
                type = null;
                break;
            case 1:
                type = 'E';
                break;
            case 2:
                type = 'S';
                break;
            case 3:
                type = 'R';
                break;
            case 4:
                type = 'W';
        }
        return type;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        Locale.setDefault(Locale.ENGLISH);

        inputDate = new JDateChooser();
        inputDate.setDateFormatString("dd-MM-yyyy");
        JTextFieldDateEditor editor = (JTextFieldDateEditor) inputDate.getDateEditor();
        editor.setEnabled(false);

        String[] timeSlots = new String[]{"", "00:00 - 03:59", "04:00 - 07:59", "08:00 - 11:59", "12:00 - 15:59", "16:00 - 19:59", "20:00 - 23:59"};
        inputTimeSlot = new JComboBox(timeSlots);
        inputTimeSlot.setSelectedIndex(0);
        inputTimeSlot.setEditable(false);

        String[] types = new String[]{"", "Earthquake", "Snow", "Rain", "Wind"};
        inputType = new JComboBox(types);
        inputType.setSelectedIndex(0);
        inputType.setEditable(false);
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        rootPanel = new JPanel();
        rootPanel.setLayout(new GridLayoutManager(4, 2, new Insets(0, 0, 0, 0), -1, -1));
        rootPanel.putClientProperty("html.disable", Boolean.TRUE);
        mainPanel = new JPanel();
        mainPanel.setLayout(new CardLayout(0, 0));
        rootPanel.add(mainPanel, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        tableDisplayPanel = new JPanel();
        tableDisplayPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(tableDisplayPanel, "tableDisplayPanelCard");
        final JScrollPane scrollPane1 = new JScrollPane();
        tableDisplayPanel.add(scrollPane1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        tableDisplay = new JTable();
        tableDisplay.setAutoCreateRowSorter(false);
        tableDisplay.setEnabled(false);
        tableDisplay.setUpdateSelectionOnSort(true);
        tableDisplay.putClientProperty("html.disable", Boolean.TRUE);
        scrollPane1.setViewportView(tableDisplay);
        final Spacer spacer1 = new Spacer();
        tableDisplayPanel.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, 1, null, null, null, 1, false));
        searchPanel = new JPanel();
        searchPanel.setLayout(new GridLayoutManager(8, 3, new Insets(0, 0, 0, 0), -1, -1));
        searchPanel.putClientProperty("html.disable", Boolean.TRUE);
        mainPanel.add(searchPanel, "searchPanelCard");
        final JLabel label1 = new JLabel();
        label1.setText("Cap");
        searchPanel.add(label1, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(80, 15), null, 1, false));
        final JLabel label2 = new JLabel();
        label2.setText("Time Slot");
        searchPanel.add(label2, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 15), null, 1, false));
        final JLabel label3 = new JLabel();
        label3.setText("Type");
        searchPanel.add(label3, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(80, 15), null, 1, false));
        searchButton = new JButton();
        searchButton.setHorizontalAlignment(0);
        searchButton.setText("Search");
        searchPanel.add(searchButton, new GridConstraints(7, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(180, 30), null, 1, false));
        inputCap = new JTextField();
        inputCap.setHorizontalAlignment(2);
        inputCap.setText("");
        searchPanel.add(inputCap, new GridConstraints(3, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(195, 15), null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Insert at least one field to search");
        searchPanel.add(label4, new GridConstraints(1, 0, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        final JSeparator separator1 = new JSeparator();
        searchPanel.add(separator1, new GridConstraints(0, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JSeparator separator2 = new JSeparator();
        searchPanel.add(separator2, new GridConstraints(2, 0, 1, 3, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(80, 15), null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Date");
        searchPanel.add(label5, new GridConstraints(6, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, 15), null, 1, false));
        searchPanel.add(inputDate, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        resetButton = new JButton();
        resetButton.setText("Reset");
        searchPanel.add(resetButton, new GridConstraints(6, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        searchPanel.add(inputTimeSlot, new GridConstraints(4, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(195, 15), null, 0, false));
        searchPanel.add(inputType, new GridConstraints(5, 1, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(195, 15), null, 0, false));
        alertDisplayPanel = new JPanel();
        alertDisplayPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(alertDisplayPanel, "alertDisplayPanelCard");
        alerDisplayTextPane = new JTextPane();
        alerDisplayTextPane.setEditable(false);
        alerDisplayTextPane.setEnabled(true);
        alerDisplayTextPane.setText("ALERT DISPLAY");
        alertDisplayPanel.add(alerDisplayTextPane, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_WANT_GROW, null, new Dimension(150, 50), null, 0, false));
        final Spacer spacer2 = new Spacer();
        alertDisplayPanel.add(spacer2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, 1, null, null, null, 1, false));
        title = new JLabel();
        Font titleFont = this.$$$getFont$$$(null, Font.BOLD | Font.ITALIC, -1, title.getFont());
        if (titleFont != null) title.setFont(titleFont);
        title.setText("Label");
        rootPanel.add(title, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 1, false));
        menuBar = new JMenuBar();
        menuBar.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        rootPanel.add(menuBar, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(80, 1), null, 0, false));
        homeMenuItem = new JMenuItem();
        homeMenuItem.setHorizontalAlignment(2);
        homeMenuItem.setHorizontalTextPosition(0);
        homeMenuItem.setMargin(new Insets(1, 1, 1, 1));
        homeMenuItem.setMaximumSize(new Dimension(-1, -1));
        homeMenuItem.setMinimumSize(new Dimension(-1, -1));
        homeMenuItem.setOpaque(false);
        homeMenuItem.setPreferredSize(new Dimension(50, 21));
        homeMenuItem.setSelected(false);
        homeMenuItem.setText("Home");
        homeMenuItem.setVerticalAlignment(0);
        homeMenuItem.setVerticalTextPosition(0);
        menuBar.add(homeMenuItem);
        searchMenu = new JMenu();
        searchMenu.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        searchMenu.setText("Search");
        menuBar.add(searchMenu);
        allDataMenuItem = new JMenuItem();
        allDataMenuItem.setHorizontalAlignment(2);
        allDataMenuItem.setHorizontalTextPosition(0);
        allDataMenuItem.setText("All Data");
        searchMenu.add(allDataMenuItem);
        seriousMenuItem = new JMenuItem();
        seriousMenuItem.setHorizontalAlignment(2);
        seriousMenuItem.setHorizontalTextPosition(0);
        seriousMenuItem.setText("Serious");
        searchMenu.add(seriousMenuItem);
        currentMenuItem = new JMenuItem();
        currentMenuItem.setHorizontalAlignment(2);
        currentMenuItem.setHorizontalTextPosition(0);
        currentMenuItem.setText("Current");
        searchMenu.add(currentMenuItem);
        customSearchMenuItem = new JMenuItem();
        customSearchMenuItem.setHorizontalAlignment(2);
        customSearchMenuItem.setHorizontalTextPosition(0);
        customSearchMenuItem.setText("Custom");
        searchMenu.add(customSearchMenuItem);
        infoCapList = new JMenuItem();
        infoCapList.setHorizontalAlignment(2);
        infoCapList.setHorizontalTextPosition(0);
        infoCapList.setText("Info Cap List");
        searchMenu.add(infoCapList);
        changeMenu = new JMenu();
        changeMenu.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        changeMenu.setText("Change");
        menuBar.add(changeMenu);
        changeCapMenuItem = new JMenuItem();
        changeCapMenuItem.setAutoscrolls(false);
        changeCapMenuItem.setHideActionText(false);
        changeCapMenuItem.setHorizontalAlignment(2);
        changeCapMenuItem.setHorizontalTextPosition(0);
        changeCapMenuItem.setLabel("Cap");
        changeCapMenuItem.setMaximumSize(new Dimension(32767, 21));
        changeCapMenuItem.setMinimumSize(new Dimension(1, 21));
        changeCapMenuItem.setOpaque(false);
        changeCapMenuItem.setRolloverEnabled(false);
        changeCapMenuItem.setSelected(false);
        changeCapMenuItem.setText("Cap");
        changeMenu.add(changeCapMenuItem);
        changeNameMenuItem = new JMenuItem();
        changeNameMenuItem.setHorizontalAlignment(2);
        changeNameMenuItem.setHorizontalTextPosition(0);
        changeNameMenuItem.setLabel("Name");
        changeNameMenuItem.setMaximumSize(new Dimension(32767, 21));
        changeNameMenuItem.setMinimumSize(new Dimension(1, 21));
        changeNameMenuItem.setPreferredSize(new Dimension(113, 21));
        changeNameMenuItem.setText("Name");
        changeMenu.add(changeNameMenuItem);
        notifyMenuItem = new JMenuItem();
        notifyMenuItem.setOpaque(false);
        notifyMenuItem.setText("Notify:");
        menuBar.add(notifyMenuItem);
        showCounterNotify = new JLabel();
        showCounterNotify.setText("0");
        menuBar.add(showCounterNotify);
        final Spacer spacer3 = new Spacer();
        rootPanel.add(spacer3, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, 1, GridConstraints.SIZEPOLICY_FIXED, new Dimension(1, 10), null, new Dimension(1, 10), 0, false));
        final Spacer spacer4 = new Spacer();
        rootPanel.add(spacer4, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_FIXED, 1, null, null, null, 1, false));
        menuItem = new ButtonGroup();
        menuItem.add(homeMenuItem);
        menuItem.add(homeMenuItem);
        menuItem.add(changeCapMenuItem);
        menuItem.add(changeNameMenuItem);
    }

    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return rootPanel;
    }


}