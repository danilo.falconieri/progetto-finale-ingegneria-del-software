package serverRMI;

import appprevisione.Allerta;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ServerAppPrevisioni extends Remote {
    ArrayList<Allerta> askAlert() throws RemoteException;
    ArrayList<Integer> getCapList() throws RemoteException;
    void stopAppPrevisioni() throws RemoteException;
}
