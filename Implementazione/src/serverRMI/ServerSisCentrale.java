package serverRMI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ServerSisCentrale extends Remote {

    ArrayList<String> listAlarm(Integer cap, Short timeSlot, Character type, String date) throws RemoteException;

    ArrayList<String> listSeriousAlarm() throws RemoteException;

    void sendUpdate(int cap, String date, short timeSlot, short level, char type) throws RemoteException;

    void removeAlert(int cap, String date, short timeSlot, char type) throws RemoteException;

    ArrayList<String> askNotice(int cap) throws RemoteException;
    
    ArrayList<String> getDefaultAlert(int cap) throws RemoteException;

    int getCounterAndIncrement() throws RemoteException;

    boolean checkInCapList(Integer cap) throws RemoteException;

    ArrayList<String> getCapList() throws RemoteException;
    
    void stopSystem() throws RemoteException;
}
