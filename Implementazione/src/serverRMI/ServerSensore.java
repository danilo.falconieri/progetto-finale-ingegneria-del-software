package serverRMI;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerSensore extends Remote {
    String askDetections() throws RemoteException;
}
