package tools;

import javax.swing.*;

import appprevisione.Sensore;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;

public class Utility {
    /* funzione per avviare il registro rmi nella build path:
     *  - se sono su windows, tenta di aprire il registro. Se non dovesse andare a buon fine, mostra il pop-up
     *  - se non sono su windows, visualizza il pop-up
     */
    public static boolean startRMIRegistry() {
        boolean manual = false;
        boolean cmdError = false;
        //chiudo tutti i registri aperti
        stopRMIRegistry();
        if (runningWindows()) {
            final String dosCommand = "cmd /c start /MIN cmd.exe /K";
            // Utility.class.getProtectionDomain().getCodeSource().getLocation().getPath() restituisce il percoso della cartella principale in cui vengono compilati tutti i file del progetto
            // ovvero il buildPath del progetto. (Per farlo, posso usare una qualsiasi classe del progetto)
            String buildPath = new File(Utility.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getAbsolutePath();
            String location =  "\"cd " + buildPath + " && start /MIN rmiregistry\"";

            // Avvio rmiregistry
            try {
                Runtime.getRuntime().exec(dosCommand + location);
            } catch (IOException e) {
                System.err.println("Error: impossible run rmiregistry");
                // e.printStackTrace();
                cmdError = true;
            }
            if (!cmdError) {
                // testo l'avvio del registro per un massimo di dieci volte. Se non si � avviato, mostro popUp di avvio manuale
                boolean exit = false;
                int i = 0;
                do {
                    try {
                        Registry reg = LocateRegistry.getRegistry("localhost");
                        reg.list();
                        exit = true;
                    } catch (RemoteException e) {
                        // e.printStackTrace();
                        if (++i == 10) {
                            manual = true;
                            exit = true;
                        }
                        System.out.println("RMIREGISTRY is starting...");
                    }
                } while (!exit);

                if (!manual) {
                    System.out.println("RMIREGISTRY started automatically");
                    return true;
                }
            }
        }
        // se manual � a true oppure non sono su windows mostro il pop-up ed effettuo un test di controllo
        if (!runningWindows() || manual) {
            // se non sono su Windows, chiedo all'utente di avviare RMIRegistry e testo che
            // sia avviato tramite il sensoreTest
            boolean exit = false;
            do {
                JFrame frame = new JFrame();
                frame.setLocationRelativeTo(null);
                frame.setAlwaysOnTop(true);
                UIManager.put("OptionPane.minimumSize", new Dimension(400, 100));
                int selection = JOptionPane.showConfirmDialog(frame,
                        "Please start manually the rmiregistry in the build folder, then press OK",
                        "Warning: RMIREGISTRY issue", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
                if (selection == JOptionPane.OK_OPTION) {
                    frame.setAlwaysOnTop(false);
                    frame.dispose();
                    try {
                        Registry reg = LocateRegistry.getRegistry("localhost");
                        reg.list();
                        //esco so se � stato avviato il registro nella cartella giusta
                        exit = true;
                    } catch (RemoteException e) {
                        // e.printStackTrace();
                        exit = false;
                    }
                } else if (selection == JOptionPane.CANCEL_OPTION) {
                    Runtime.getRuntime().exit(0);
                }
            } while (!exit);
        }
        return false;
    }

    public static boolean stopRMIRegistry() {
        if (runningWindows()) {
            try {
                // before open new cmd + rmiregistry, if they are opened, it kills them
                Process p1 = Runtime.getRuntime().exec("taskkill /f /im cmd.exe");
                Process p2 = Runtime.getRuntime().exec("taskkill /f /im rmiregistry.exe");
                while (p1.isAlive() || p2.isAlive()) {
                    // fase di attesa che i processi di chiusura siano terminati, cio� abbiano
                    // killato tutti i processi cmd e rmiregistry
                }
                return true;
            } catch (IOException e) {
                // e.printStackTrace();
                return false;
            }
        } else {
            // testo se � aperto rmi registry se non lo � ritorno true
            try {
                Registry reg = LocateRegistry.getRegistry("localhost");
                reg.list();
            } catch (RemoteException e) {
                // e.printStackTrace();
                return true;
            }
            // se � aperto mostro il pop-up
            boolean exit = false;
            do {
                JFrame frame = new JFrame();
                frame.setLocationRelativeTo(null);
                frame.setAlwaysOnTop(true);
                UIManager.put("OptionPane.minimumSize", new Dimension(400, 100));
                int selection = JOptionPane.showConfirmDialog(frame,
                        "Please stop manually the rmiregistry, then press OK", "Warning: stop rmiregistry",
                        JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
                if (selection == JOptionPane.OK_OPTION) {
                    frame.setAlwaysOnTop(false);
                    frame.dispose();
                    // testo che sia effettivamente stato chiuso
                    try {
                        Registry reg = LocateRegistry.getRegistry("localhost");
                        reg.list();
                        exit = false;
                    } catch (RemoteException e) {
                        // e.printStackTrace();
                        exit = true;
                    }
                } else if (selection == JOptionPane.CANCEL_OPTION) {
                    Runtime.getRuntime().exit(0);
                }
            } while (!exit);
        }
        return false;
    }

    private static boolean runningWindows() {
        return System.getProperty("os.name").contains("Windows");
    }
    
    public static ArrayList<ArrayList<Integer>> deleteDuplicates(ArrayList<ArrayList<Integer>> caps) {
    	ArrayList<ArrayList<Integer>> newCaps = new ArrayList<>();
    	for(int i=0;i<caps.size();i++) 
    		newCaps.add(new ArrayList<Integer>());
    	
    	for (int i = 0; i < caps.size(); i++) {
            for (Integer cap : caps.get(i)) {
            	boolean duplicate = false;
            	for(int j=0;j<caps.size();j++) {
            		if(newCaps.get(j).contains(cap)) {
            			duplicate = true;
            			break;
            		}
            	}
                if(duplicate == false) {
                	newCaps.get(i).add(cap);
                }
            }
        }
    	return newCaps;
    }

}

