
import appprevisione.AppPrevisioni;
import appprevisione.Sensore;
import protezionecivile.SistemaCentrale;
import tools.Utility;
import user.AppMobileCreator;
import user.AppMobileWindow;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		SistemaCentrale centralSysyem = null;
		ArrayList<AppPrevisioni> apps = new ArrayList<>();

		ArrayList<String> appList = new ArrayList<>();
		appList.add("AppPrevisione.Friuli");
		appList.add("AppPrevisione.Lombardia");
		appList.add("AppPrevisione.Puglia");
		appList.add("AppPrevisione.Calabria");

		ArrayList<ArrayList<Integer>> caps = new ArrayList<>();
		caps.add(new ArrayList<>(Arrays.asList(34071, 34070))); // Lista cap Friuli
		caps.add(new ArrayList<>(Arrays.asList(22100, 22078))); // Lista cap Lombardia
		caps.add(new ArrayList<>(Arrays.asList(73100, 70132))); // Lista cap Puglia
		caps.add(new ArrayList<>(Arrays.asList(88100, 89133))); // Lista cap Calabria

		caps = Utility.deleteDuplicates(caps);

		Utility.startRMIRegistry();

		centralSysyem = SistemaCentrale.getInstance();

		// Creazione sensori
		for (int i = 0; i < Math.min(caps.size(), appList.size()); i++) {
			for (Integer cap : caps.get(i)) {
				Sensore s1 = new Sensore(cap, 'S');
				Sensore s2 = new Sensore(cap, 'W');
				Sensore s3 = new Sensore(cap, 'R');
				Sensore s4 = new Sensore(cap, 'E');
			}
		}

		ArrayList<String> appListOfficial = new ArrayList<String>();
		appListOfficial.addAll(appList);
		int iterator = 0;
		for (String id : appList) {
			try {
				apps.add(new AppPrevisioni(id, caps.get(iterator)));
			} catch (Exception e) {
				appListOfficial.remove(id);
			}
			iterator++;
		}

		centralSysyem.setAppList(appListOfficial);

		AppMobileCreator amc = AppMobileCreator.getInstance();

		// Porzione di codice che crea automaticamente un'AppMobile per ogni cap
		/*
		for (int i = 0; i < appListOfficial.size(); i++) {
			for (int j = 0; j < caps.get(i).size(); j++) {
				String name = "USER";
				name += Integer.toString(i) + Integer.toString(j);
				new AppMobileWindow(caps.get(i).get(j), name);
			}
		}
		*/

	}
}
